/*
 * sidebar controller here
 *
 */
(function() {
    'use strict';

    function sideBarController($scope, $log, $state, ngUtils, Notify, Api, APP_CONFIG) {
        var vm = this;

        function clearForm(){

        	vm.passOld = '';
			vm.passNew = '';
			vm.passNew2 = '';

        };

		vm.getUsers = function() {
			console.info('vm.dataUser***', vm.dataUser);
			Api('user/'+vm.dataUser.data.id, 'GET').then((function(result) {
				console.info('getUsers user/ result.status', result.status);
				switch (result.status) {
					case 200:
						console.log('result.data.info+++',result.data);
						vm.listUsers = result.data;
						vm.nameUser = MaysPrimera(result.data.first_name)+" "+MaysPrimera(result.data.last_name);

						var arrayAvatar = [
							'images/avatars/man.svg','images/avatars/man-1.svg','images/avatars/man-2.svg','images/avatars/man-3.svg',
							'images/avatars/woman.svg','images/avatars/woman-1.svg','images/avatars/woman-2.svg','images/avatars/woman-3.svg'
						];

						var cont = 0;
						//angular.forEach(vm.listUsers, function(value, index){

							if(cont==7){
								cont=0;
							}

						vm.listUsers.avatar = arrayAvatar[1];
						console.info('angular.forEach value', vm.listUsers.avatar);

							cont++;

						//});

						break;
					default:
						var _msg = 'Las credenciales de autenticación no se proporcionaron';
						Notify.send(_msg, {
							status: 'warning',
							timeout: 4500
						});
				}
			}), function(error) {
				switch (error) {
					case 400:
					case 401:
						var _msg = 'Las credenciales de autenticación no se proporcionaron';
						Notify.send(_msg, {
							status: 'warning',
							timeout: 4500
						});
						break;
					default:
						console.log("error inesperado. user_get");
				}
			});
		};

		function MaysPrimera(string){
			return string.charAt(0).toUpperCase() + string.slice(1);
		}

        vm.changePassword = function() {

            if (vm.passOld != '' && vm.passNew != '' && vm.passNew2 != '') {

	            if (vm.passNew == vm.passNew2) {

	            	var obj = {
						new_password:vm.passNew2,
						old_password:vm.passOld
					}


        			$("#changePassword").button('loading');
	            	Api('user/pass/', 'POST', obj).then((function(result) {
                        switch (result.status) {
                            case 200:
                            case 201:
                                Notify.send('Contraseña actualizada con éxito.', {
                                    status: 'success',
                                    timeout: 5000
                                });
                                clearForm();
                                break;
                            default:
                                var _msg = 'Respuesta inesperada. Por favor intente nuevamente. Gracias';
                                Notify.send(_msg, {
                                    status: 'warning',
                                    timeout: 5000
                                });
                        }
                        $("#changePassword").button('reset');
                    }), function(error) {
                        switch (error) {
                            case 401:
                                var _msg = 'La contraseña actual no coincide.';
                                Notify.send(_msg, {
                                    status: 'warning',
                                    timeout: 4500
                                });
                                break;
                            default:
                                console.log("error inesperado. user_get");
                        }
                        $("#changePassword").button('reset');
                    });


	            } else {
	                Notify.send('Las contraseñas nuevas no coincide.', {
	                    status: 'warning',
	                    timeout: 5000
	                });
	            }


            } else {
                Notify.send('Por favor valide los datos.', {
                    status: 'info',
                    timeout: 5000
                });
            }
        };

        function logout(){

        	var campos = {};
        	campos.user = JSON.parse(sessionStorage.user).data.cedula;

        		Api('user/Logout/', 'POST', campos).then((function(result) {
	                $log.log('Api.post login', result);

	                switch (result.status) {
	                    case 200:
	                    	delete sessionStorage.session;
	                    	delete sessionStorage.user;
	                        $state.go('app.slogin');
							setTimeout("location.reload()", 100);
	                        break;
	                    default:
                            Notify.send('Ocurrio un error por favor intente nuevamente.', {
                                status: 'warning',
                                timeout: 4500
                            });

	                }
	            }), function(error) {

	            	switch(error){
	            		case 400:
	            		case 401:
	            			Notify.send("Ocurrio un error por favor intente nuevamente.", {
                                status: 'warning',
                                timeout: 4500
                            });
	            			break;
	            		default:
	                		$log.error('Ocurrio un error por favor intente nuevamente.', error);
	            	}

	            });

        }

        vm.viewContent = function(option) {

            switch (option) {
                case 'groups':
                    $state.go('app.groups');
                    vm.hideSidebar();
                    break;
                case 'area':
                    vm.hideSidebar();
					vm.objClear();
                    $state.go('app.areas');
                    break;
				case 'temas':
					vm.objClear();
					$state.go('app.temas');
					vm.hideSidebar();
					break;
				case 'items':
					vm.objClear();
					$state.go('app.items');
					vm.hideSidebar();
					break;
				case 'problematicas':
					vm.objClear();
					$state.go('app.problematicas');
					vm.hideSidebar();
					break;
				case 'formularios':
					vm.objClear();
					$state.go('app.formularios');
					vm.hideSidebar();
					break;
				case 'logout':
					localStorage.removeItem('session');
					vm.hideSidebar();
					logout();
					break;
                default:
                    console.log("default");

            }

        };

		vm.objClear = function() {
			delete sessionStorage.objInfo;
			var obj = JSON.stringify({});
			sessionStorage.setItem("objInfo",obj);

			$state.go("app.items");

		};

        vm.hideSidebar = function() {
            ngUtils.sideBar('hide');
        }


        vm.main = function() {
            $log.info('sidebar Controller');
			//setTimeout("location.reload()", 1000);
			vm.typeuser = sessionStorage.typeuser;
			vm.dataUser = JSON.parse(sessionStorage.user);
			//$log.info('dataUser**', vm.dataUser);
			vm.getUsers();
            vm.hideSidebar();
            clearForm();
        };

        vm.main();
    }

    angular.module('app.core').controller('sideBarController', sideBarController);
    sideBarController.$inject = [
        '$scope',
        '$log',
        '$state',
        'ngUtils',
        'Notify',
        'Api',
        'APP_CONFIG'
    ];
})();
