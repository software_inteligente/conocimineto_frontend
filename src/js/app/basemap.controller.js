/*
 * baseMap controller here
 *
 */
(function() {
        'use strict';

	function baseMapController($scope, $timeout, $log, Api, Notify, APP_CONFIG, $state) {
		var vm = this;

		var currentPos = {
			'lat': 4.672454,
			'lng': -74.064038
		};

		var currentZoom = 14;

		var refUsers;

		vm.toggleSideBar = true;
		vm.toggleArrow = true;
		vm.textArrow = true;

		vm.toggleSideBar2 = false;
		vm.toggleArrow2 = false;
		vm.textArrow2 = false;

		vm.viewRoutes = true;
		vm.viewAlarms = false;

		vm.listCustomerMap = [];
		vm.listTareaMap = [];
		vm.listAlarm = [];
		vm.alarmType = function() {

                vm.listAlarm = [
                    { id: 1, usuario: 'Karen Gomez Gomez', alerta: 'Gps On', fecha: '2017-07-02 09:56', direccion: 'cll 84 # 24 -78 este.', lat: 4.35145, lng: -74.02245 },
                    { id: 2, usuario: 'Felipe romero', alerta: 'Gps Off', fecha: '2017-07-02 09:56', direccion: 'cll 84 # 24 -78 este.', lat: 4.42145, lng: -74.03245 },
                    { id: 3, usuario: 'Alfredo neme', alerta: 'Ausencia de transmisión', fecha: '2017-07-02 09:56', direccion: 'cll 84 # 24 -78 este.', lat: 4.52145, lng: -74.04245 },
                    { id: 4, usuario: 'Camilo Romero Romero', alerta: 'Detencion Prolongada', fecha: '2017-07-02 09:56', direccion: 'cll 84 # 24 -78 este.', lat: 4.30145, lng: -74.05245 },
                    { id: 5, usuario: 'Jhonatan Torres', alerta: 'Entrada de zona', fecha: '2017-07-02 09:56', direccion: 'cll 84 # 24 -78 este.', lat: 4.39845, lng: -74.06245 },
                    { id: 6, usuario: 'William Marin', alerta: 'Fuera de zona', fecha: '2017-07-02 09:56', direccion: 'cll 84 # 24 -78 este.', lat: 4.38845, lng: -74.07245 },
                    { id: 7, usuario: 'Carlos Matiz', alerta: 'otra', fecha: '2017-07-02 09:56', direccion: 'cll 84 # 24 -78 este.', lat: 4.015145, lng: -74.08245 }
                ];

                angular.forEach(vm.listAlarm, function(value, index) {

                    switch (value.alerta) {
                        case 'Gps On':
                            value.icon = 'gps_fixed';
                            value.color = 'colorGpsOn'; //009688
                            break;
                        case 'Gps Off':
                            value.icon = 'gps_off';
                            value.color = 'colorGpsOff'; //607D8B
                            break;
                        case 'Ausencia de transmisión':
                            value.icon = 'signal_cellular_off';
                            value.color = 'colorAusenciaTransmisión'; //#ff9800
                            break;
                        case 'Detencion Prolongada':
                            value.icon = 'report';
                            value.color = 'colorDetencionProlongada'; //#795548
                            break;
                        case 'Entrada de zona':
                            value.icon = 'transfer_within_a_station';
                            value.color = 'colorEntradaZona'; //00bcd4
                            break;
                        case 'Fuera de zona':
                            value.icon = 'directions_run';
                            value.color = 'colorFueraZona'; //2196F3
                            break;
                        default:
                            value.icon = 'notifications_active';
                            value.color = 'colorDefault'; //2196F3
                    }

                });
            };


		vm.showView = function(option) {

			vm.viewRoutes = false;
			vm.viewAlarms = false;
			switch (option) {
				case 'routes':
					vm.viewRoutes = true;
					break;
				case 'alarms':
					vm.viewAlarms = true;
					break;
				default:
					console.log("opcion no encontrada vista items mapas")
			}
		};

		vm.addMarker = function(option, idUser, idCustomer) {

                Anuket.removeMarkers();

                var bounds = new google.maps.LatLngBounds();

                switch(option){
                	case 'user':
                		console.info('vm.listCustomerMap user', vm.listCustomerMap);
                		angular.forEach(vm.listCustomerMap, function(value, index) {

							 if (value.id === idUser) {
	                        	var jsonData = {};
								jsonData.action = "USER";
								jsonData.user = idUser;
								currentPosicion(jsonData, value);
	                        }

	                    });
                		break;
                	case 'userAll':
							var jsonData = {};
							jsonData.action = "ALL";
							Api('currentposition/', 'POST', jsonData).then((function(result) {
								//console.log('result all', result);
								angular.forEach(result.data.info, function(value, index) {
									console.log('result all for', value);
									var Obj_position = value;
									angular.forEach(vm.listCustomerMap, function(valueuser, indexuser) {
										if(parseInt(valueuser.id)===parseInt(Obj_position.user)){
											console.log('result vm.typemarker(value.date)', vm.typemarker(value.date));
											console.log('result all valueuser.nombre', valueuser.nombre);
											var label = valueuser.nombre.substring(0, 1).toUpperCase();
											var nombre = vm.ucWords(valueuser.nombre);
											//console.info(nombre);
											var objSend = { longitude: Obj_position.point.lon, latitude: Obj_position.point.lat };

											Anuket.moveMarkerCustomer = false;
											console.info('objSend', objSend);
											Api('sitidata/geoinverso/', 'POST', objSend).then((function(resultgeo) {
												console.info('result sitidata/geoinverso/******', resultgeo);
												switch (result.status) {
													case 200:
													case 201:
														if (resultgeo.data.latitude != 0 && resultgeo.data.longitude != 0) {
															/*Notify.send("Geo localización exitosa..", {
															 status: 'success',
															 timeout: 3500
															 });*/

															var dir = resultgeo.data.address1;
															console.info('dir***', dir);

															var content = '<div class="infoWindowsContent"><b>Nombre:</b> '+nombre+'<br/><b>Dirección</b> ' + dir + '<br><b>Fecha/Hora: </b>'+value.date+'<br/><img class="imgstreetview" src="images/markers/streetview.png" onclick="Anuket.streetView2('+Obj_position.point.lon+', '+Obj_position.point.lat+')">';

															Anuket.addMarker({ "lat": Obj_position.point.lat, "lng": Obj_position.point.lon }, false, content, null, null, vm.typemarker(value.date), label, 'users');
															Anuket.centerMap(Obj_position.point.lat, Obj_position.point.lon);

															var myLatLngCenter = new google.maps.LatLng(parseFloat(Obj_position.point.lat), parseFloat(Obj_position.point.lon), Obj_position);
															bounds.extend(myLatLngCenter);
															setTimeout(function () {
																$('#imgstreetview').on('click', function() {
																	console.info('hooskjskskskskkssks');
																});
															},1000)

														} else {
															vm.campos.cx = '';
															vm.campos.cy = '';
															Notify.send("Error al buscar la dirección. Por favor ubique el punto de forma manual.", {
																status: 'warning',
																timeout: 6000
															});
														}

														break;
													default:
														var _msg = 'Las credenciales de autenticación no se proporcionaron';
														Notify.send(_msg, {
															status: 'warning',
															timeout: 4500
														});
												}
											}), function(error) {
												switch (error) {
													case 400:
													case 401:
														var _msg = 'Las credenciales de autenticación no se proporcionaron';
														Notify.send(_msg, {
															status: 'warning',
															timeout: 4500
														});
														break;
													case 403:
														var _msg = 'No tiene permisos para consultar el Geo.';
														Notify.send(_msg, {
															status: 'warning',
															timeout: 4500
														});
														break;
													default:
														console.log("error inesperado. user_get");
												}
											});
										}
									});
								});
							}), function(error) {
								switch (error) {
									case 400:
									case 401:
										var _msg = 'Las credenciales de autenticación no se proporcionaron';
										Notify.send(_msg, {
											status: 'warning',
											timeout: 4500
										});
										break;
									case 403:
										var _msg = 'No tiene permisos para consultar el Geo.';
										Notify.send(_msg, {
											status: 'warning',
											timeout: 4500
										});
										break;
									default:
										console.log("error inesperado. user_get");
								}
							});
                		break;
                	case 'customer':
	                	angular.forEach(vm.listCustomerMap, function(value, index) {

	                        if (value.id === idUser) {

	                            angular.forEach(value.clientes, function(value2, index2) {

	                                if (value2.id === idCustomer) {

	                                    var icon = '';

	                                    if (value2.estado === "visitado") {
	                                        icon = 'images/markers/visitado.png';
	                                    } else {
	                                        icon = 'images/markers/novisitado.png';
	                                    }

	                                    Anuket.addMarker({ "lat": value2.coordenadas[0], "lng": value2.coordenadas[1] }, false, null, null, null, icon, '', 'customers');
	                                    Anuket.centerMap(value2.coordenadas[0], value2.coordenadas[1]);

	                                    var myLatLngCenter = new google.maps.LatLng(parseFloat(value2.coordenadas[0]), parseFloat(value2.coordenadas[1]), value2);
	                                    bounds.extend(myLatLngCenter);
	                                }
	                            });

	                        }

	                    });
                		break;
                	case 'allCustomer':
                		console.info('vm.listCustomerMap**', vm.listCustomerMap);
	                	angular.forEach(vm.listCustomerMap, function(value, index) {

	                        var label = value.nombre.substring(0, 1).toUpperCase();
	                        var content = '<div class="infoWindowsContent"><b>Dirección: </b>' + value.direccion + '<br><b>Nombre: </b>' + value.nombre + '</div>';

	                        if (value.id === idUser) {
	                            var arrayWaypoints = [];
	                            if(jQuery.isEmptyObject(value.clientes)){
									var start = { lat: 4.735885, lng: -74.076347 };
									var end = { lat: 4.735885, lng: -74.076347 };
								}else{
									console.info('allCustomer value', value.clientes);
									var arrayListClientDay = [];
									var cont2=0;
									angular.forEach(value.clientes, function(value2, index2) {
										if(value2.date_prog === vm.myDate2()){
											arrayListClientDay[cont2] = value2;
											if (index2 != 0 && index2 != value.clientes.length - 1) {
												arrayWaypoints.push({
													location: value2.coordenadas[0] + ',' + value2.coordenadas[1],
													stopover: true
												});
											}
											cont2++;
										}
									});
									value.clientes = arrayListClientDay;

									var start = { lat: parseFloat(value.clientes[0].coordenadas[0]), lng: parseFloat(value.clientes[0].coordenadas[1]) };
									var end = { lat: parseFloat(value.clientes[value.clientes.length - 1].coordenadas[0]), lng: parseFloat(value.clientes[value.clientes.length - 1].coordenadas[1]) };

								}

	                            //markers and route Customers of User
	                            Anuket.directions(start, end, arrayWaypoints, 'DRIVING', function(response) {

	                                var order = response.routes[0].waypoint_order;
	                                var n = 0;

	                                angular.forEach(value.clientes, function(value2, index2) {
	                                    var max = value.clientes.length;

	                                    var icon = '';
	                                    var colorText = '';

	                                    if (value2.estado === "visitado") {
	                                        icon = 'images/markers/circleGreen.png';
	                                        colorText = 'colorVisit';
	                                    } else if (value2.estado === "no visitado") {
	                                        icon = 'images/markers/circleRed.png';
	                                        colorText = 'colorNoVisit';
	                                    } else {
	                                        icon = 'images/markers/circleYellow.png';
	                                        colorText = 'colorTransit';
	                                    }
										/******/
										console.info('index2', index2);
	                                    console.info('value2', value2);
										console.info('value2.direccion', value2.direccion);
	                                    var content2 = '<div class="infoWindowsContent"><b>Dirección: </b>' + value2.direccion +
	                                        '<br><b><b>Estado: </b> <b class="' + colorText + '">' + value2.estado + '</b><br><b>Cliente: </b>' + value2.nombre + '' +
											'<br/><img class="imgstreetview" src="images/markers/streetview.png" onclick="Anuket.streetView2('+value2.coordenadas[1]+', '+ value2.coordenadas[0]+')"></div>';

	                                    if (index2 === 0) {
	                                        //markert start route
	                                        Anuket.addMarker({ "lat": value2.coordenadas[0], "lng": value2.coordenadas[1] }, false, content2, null, null, icon, '1', 'route');
	                                    } else if (index2 != 0 && index2 != max - 1) {
	                                        //markert waypoints route
	                                        Anuket.addMarker({ "lat": value2.coordenadas[0], "lng": value2.coordenadas[1] }, false, content2, null, null, icon, (order[n] + 2).toString(), 'route');
	                                        n = n + 1;
	                                    } else {
	                                        //markert end route
	                                        Anuket.addMarker({ "lat": value2.coordenadas[0], "lng": value2.coordenadas[1] }, false, content2, null, null, icon, max.toString(), 'route');
	                                    }

	                                });

	                                //marker User
	                                Anuket.addMarker({ "lat": value.posicion[0], "lng": value.posicion[1] }, false, content, null, null, 'images/markers/circleBlue.png', label, 'users');

	                                /******/

	                            });
	                        }

	                    });
                		break;
                	case 'allRoute':
						var route =[];
	                    angular.forEach(vm.listCustomerMap, function(value, index) {
	                        if (value.id === idUser) {
								if(jQuery.isEmptyObject(value.clientes)){
									console.info('SI***');
									route = [];
								}else{
									console.info('NO***');
									var cont = 0;
									angular.forEach(value.clientes, function(value2, index2) {
										if(value2.date_prog === vm.myDate2()){
											var latLog = {};
											latLog.lat = parseFloat(value2.coordenadas[0]);
											latLog.lng = parseFloat(value2.coordenadas[1]);
											route[cont] = latLog;
											cont++;
										}

									});
								}

	                            value.route = route;

	                            angular.forEach(value.route, function(value2, index2) {

									var content = '<div class="infoWindowsContent"><b>Dirección: </b>' + value.direccion + '<br><b>' + value.nombre + '</b>'
										+'<br/><img class="imgstreetview" src="images/markers/streetview.png" onclick="Anuket.streetView2('+value2.lng+', '+value2.lat+')"></div>';

									var label = value.nombre.substring(0, 1).toUpperCase();
	                                Anuket.addMarker({ "lat": value2.lat, "lng": value2.lng }, false, content, null, null, 'images/markers/circleBrown.png', label, 'userHistorical');
	                                var myLatLngCenter = new google.maps.LatLng(parseFloat(value2.lat), parseFloat(value2.lng), value2);
	                                bounds.extend(myLatLngCenter);

	                            });

	                            Anuket.polyLines(value.route, '#ff9800');
	                        }

	                    });
                		break;
                	case 'alarms':
                		angular.forEach(vm.listAlarm, function(value, index) {

	                            if (value.id === idUser) {

	                                var icon = 'images/markers/'+value.alerta+'1.png';

	                                var exists = existeUrl(icon);

	                                if(!exists){
	                                	icon = 'images/markers/otra.png';
	                                }

	                            	var content = '<div class="infoWindowsContent"><h5><b>Dirección</b></h5>' + value.direccion + '<br>'+
	                            				  '<i class="material-icons '+value.color+'">'+value.icon+'</i><br>'+
	                            				  '<b>'+value.alerta+'</b><br><br>'+
	                            				  '<span>'+value.fecha+'</span><br><br><b>' + value.usuario + '</b></div>';

	                                Anuket.addMarker({ lat: value.lat, lng: value.lng}, false, content, null, null, icon, ' ', 'alarms');
	                            	Anuket.centerMap(value.lat, value.lat);

	                            var myLatLngCenter = new google.maps.LatLng(value.lat, value.lng, value);
	                            bounds.extend(myLatLngCenter);
	                        }
	                    });
                		break;
                	case 'alarmsAll':
                		angular.forEach(vm.listAlarm, function(value, index) {

							var icon = 'images/markers/'+value.alerta+'1.png';

							var exists = existeUrl(icon);

                            if(!exists){
                            	icon = 'images/markers/otra.png';
                            }

	                        var content = '<div class="infoWindowsContent"><h5><b>Dirección</b></h5>' + value.direccion + '<br>'+
	                        			  '<i class="material-icons '+value.color+'">'+value.icon+'</i><br>'+
	                            		  '<b>'+value.alerta+'</b><br><br>'+
	                            		  '<span>'+value.fecha+'</span><br><br><b>' + value.usuario + '</b></div>';

	                        Anuket.addMarker({ lat: value.lat, lng: value.lng}, false, content, null, null, icon, ' ', 'alarms');
	                    	//Anuket.centerMap(value.lat, value.lat);

	                        var myLatLngCenter = new google.maps.LatLng(value.lat, value.lng, value);
	                        bounds.extend(myLatLngCenter);

	                    });
                		break;
                }

            Anuket.getObjectMap().map.fitBounds(bounds);
        };

		vm.assignHomework = function (idTarea) {
			$('.selectpicker').selectpicker('refresh');
			vm.loadMap();
			$('#createCustomer2').modal('show');
		}

		/**
		 * @author jorge Angarita
		 * @function Activar el Mapa, se debe colocar en el metodo donde se valla amostrar el Mapa
		 *
		 */
		vm.loadMap = function() {
			$("#createCustomer2").on("shown.bs.modal", function(e) {
				//Anuket.streetView(coords);
				Anuket.run('#mapCustomer2', {
					'zoom': 12,
					'latlng': { "lat": 4.6725634, "lng": -74.064028 },
					'initMarker': false
				});

				Anuket.removeMarkers();
				Anuket.addMarker({ "lat": parseFloat(4.6725634), "lng": parseFloat(-74.064028) }, true, 'createCustomer2', null);
			});
		}


		vm.getStreetView = function () {
			console.info('streetViewModal');
			/*$("#streetViewModal").modal("show");
			var lat = parseFloat(lat);
			var lng = parseFloat(lon);


			setTimeout(initialize, 200);*/

		};

		function initialize() {
			var panorama = new google.maps.StreetViewPanorama(
				document.getElementById('streetViewModalContent'), {
					position: new google.maps.LatLng(lat, lng),
					pov: {
						heading: 24,
						pitch: 10
					}
				});
			map.setStreetView(panorama);
		}

	    function existeUrl(url) {
			var http = new XMLHttpRequest();
			http.open('HEAD', url, false);
			http.send();
			return http.status!=404;
		};

	    function currentPosicion(jsonData, infoUser) {
			var bounds = new google.maps.LatLngBounds();
			Api('currentposition/', 'POST', jsonData).then((function(result) {
				switch (result.status) {
					case 200:
					console.info('currentposition/ data',result.data);
					console.info('currentposition/ fecha',moment(result.data.date,'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD') );
						if(jQuery.isEmptyObject(result.data)){
							Notify.send("No tienen posición actual", {
								status: 'warning',
								timeout: 6000
							});
							break;
						}

						/*if(moment(result.data.date,'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD') !== vm.myDate2()){
							Notify.send("No tienen posición actual para hoy", {
								status: 'warning',
								timeout: 6000
							});
							break;
						}*/
					var jsonData = {};
						jsonData.cx = result.data.point.lat;
						jsonData.cy = result.data.point.lon;
					var date_repoart = 	result.data.date;

						//vm.onGeoInverso(jsonData,jsonData);
						var cx = jsonData.cx;
						var cy = jsonData.cy;

						var objSend = { longitude: cy, latitude: cx };
						//console.info('objSend', objSend);
						Anuket.moveMarkerCustomer = false;

						Api('sitidata/geoinverso/', 'POST', objSend).then((function(result) {
							console.info('sitidata/geoinverso/ result', result);

							switch (result.status) {
								case 200:
								case 201:
									if (result.data.address1 != '' && result.data.address1 != undefined) {

										result.data.direccion = result.data.address1;
										var label = infoUser.nombre.substring(0, 1).toUpperCase();
										var content = '<div class="infoWindowsContent"><b>Dirección: </b>' + result.data.direccion+ '<br><b>Nombre: </b>' + infoUser.nombre + '<br><b>Fecha/Hora: </b>'+date_repoart+'<br/><img class="imgstreetview" src="images/markers/streetview.png" onclick="Anuket.streetView2('+objSend.longitude+', '+objSend.latitude+')"></div>';

										Anuket.addMarker({ "lat": objSend.latitude, "lng": objSend.longitude }, false, content, null, null, 'images/markers/circleBlue.png', label, 'users');
										Anuket.centerMap(objSend.latitude, objSend.longitude);

										var myLatLngCenter = new google.maps.LatLng(parseFloat(objSend.latitude), parseFloat(objSend.longitude), infoUser);
										bounds.extend(myLatLngCenter);


									} else {
										vm.campos.cx = '';
										vm.campos.cy = '';
										Notify.send("Error al buscar la dirección. Por favor ubique el punto de forma manual.", {
											status: 'warning',
											timeout: 6000
										});
									}

									break;
								default:
									var _msg = 'Las credenciales de autenticación no se proporcionaron';
									Notify.send(_msg, {
										status: 'warning',
										timeout: 4500
									});
							}
						}), function(error) {
							switch (error) {
								case 400:
								case 401:
									var _msg = 'Las credenciales de autenticación no se proporcionaron';
									Notify.send(_msg, {
										status: 'warning',
										timeout: 4500
									});
									break;
								case 403:
									var _msg = 'No tiene permisos para consultar el Geo.';
									Notify.send(_msg, {
										status: 'warning',
										timeout: 4500
									});
									break;
								default:
									console.log("error inesperado. user_get");
							}
						});
						break;
					default:
						var _msg = 'Las credenciales de autenticación no se proporcionaron';
						Notify.send(_msg, {
							status: 'warning',
							timeout: 4500
						});
				}
			}), function(error) {
				switch (error) {
					case 400:
					case 401:
						var _msg = 'Las credenciales de autenticación no se proporcionaron';
						Notify.send(_msg, {
							status: 'warning',
							timeout: 4500
						});
						break;
					case 403:
						var _msg = 'No tiene permisos para consultar el Geo.';
						Notify.send(_msg, {
							status: 'warning',
							timeout: 4500
						});
						break;
					default:
						console.log("error inesperado. user_get");
				}
			});
		}

		/**
		 * @author jorge Angarita
		 * @function Colocar la primera letra de cada palabra en mayuscula
		 *
		 */
		vm.ucWords= function (string){
			var arrayWords;
			var returnString = "";
			var len;
			//console.info('ucWords',string);
			arrayWords = string.split(" ");
			len = arrayWords.length;
			for(var i=0;i < len ;i++){
				if(i != (len-1)){
					returnString = returnString+vm.ucFirst(arrayWords[i])+" ";
				}
				else{
					returnString = returnString+vm.ucFirst(arrayWords[i]);
				}
			}
			return returnString;
		};

		/**
		 * @author jorge Angarita
		 * @function Segunda para de Colocar la primera letra de cada palabra en mayuscula
		 *
		 */
		vm.ucFirst= function(string){
			return string.substr(0,1).toUpperCase()+string.substr(1,string.length).toLowerCase();
		};
		/**Infomación de usuario**/

	    vm.getUsers = function() {
			vm.listCustomerMap = [];
				Api('user/', 'GET').then((function(result) {
					switch (result.status) {
						case 200:
							vm.listUsers = result.data.info;
							console.info('listUsers**', vm.listUsers);
							angular.forEach(vm.listUsers, function(value, index){
								var jsonInfo = {};
								console.info('value**', value);
								console.info('index**', index);
								jsonInfo.id= value.id;
								jsonInfo.nombre= value.first_name+" "+value.last_name;
								jsonInfo.telefono = value.telefono;
								jsonInfo.bateria = "12";
								jsonInfo.imagen = 'https://image.flaticon.com/icons/svg/201/201634.svg';
								jsonInfo.posicion = [4.735885, -74.076347];
								//0jsonInfo.direccion = 'cll 78 # 24 - 26 oeste';
								jsonInfo.visitados = 12;
								jsonInfo.novisitados = 9;
								jsonInfo.cedula = value.cedula;;
								var jsonData = {
									"user_id": value.id,
									"end_date": vm.myDate2()
								};
								// Lista los evtontos creados en el calendario
								vm.getListEvent(jsonData, jsonInfo);

								vm.listCustomerMap[index]= jsonInfo;
							});
							console.info('vm.listCustomerMap', vm.listCustomerMap);
							break;
						default:
							var _msg = 'Las credenciales de autenticación no se proporcionaron';
							Notify.send(_msg, {
								status: 'warning',
								timeout: 4500
							});
					}
				}), function(error) {
					switch (error) {
						case 400:
						case 401:
							var _msg = 'Las credenciales de autenticación no se proporcionaron';
							Notify.send(_msg, {
								status: 'warning',
								timeout: 4500
							});
							break;
						default:
							console.log("error inesperado. user_get");
					}
				});

			Api('http://localhost/proyecto_praxys/prueba/SitiDoctor/public/api/CrearTareas', 'GET', null, {'external': true}).then((function(result) {
				switch (result.status) {
					case 200:
						vm.listTareas = result.data;
						console.info('listTareas**', vm.listTareas);
						angular.forEach(vm.listTareas, function(value, index){
							;

							vm.listTareaMap[index]= value;
						});
						console.info('vm.listTareaMap', vm.listTareaMap);
						break;
					default:
						var _msg = 'Las credenciales de autenticación no se proporcionaron';
						Notify.send(_msg, {
							status: 'warning',
							timeout: 4500
						});
				}
			}), function(error) {
				switch (error) {
					case 400:
					case 401:
						var _msg = 'Las credenciales de autenticación no se proporcionaron';
						Notify.send(_msg, {
							status: 'warning',
							timeout: 4500
						});
						break;
					default:
						console.log("error inesperado. user_get");
				}
			});
			};

		vm.getCustomers = function() {
			Api('client/', 'GET').then((function(result) {
				switch (result.status) {
					case 200:

						vm.listCustomers = result.data.info;
						console.log("vm.listCustomers", vm.listCustomers);

						break;
					default:
						var _msg = 'Las credenciales de autenticación no se proporcionaron';
						Notify.send(_msg, {
							status: 'warning',
							timeout: 4500
						});
				}
			}), function(error) {
				switch (error) {
					case 400:
					case 401:
						var _msg = 'Las credenciales de autenticación no se proporcionaron';
						Notify.send(_msg, {
							status: 'warning',
							timeout: 4500
						});
						break;
					case 403:
						var _msg = 'No tiene permisos para consultar los Clientes.';
						Notify.send(_msg, {
							status: 'warning',
							timeout: 4500
						});
						break;
					default:
						console.log("error inesperado. user_get");
				}
			});
		};

		vm.getCompany = function() {
			vm.listCompany = [{id:JSON.parse(sessionStorage.user).data.id_empresa,nombre_empresa:'Empresa '+JSON.parse(sessionStorage.user).data.id_empresa}]
			console.info('vm.listCompany**', vm.listCompany);
			//vm.listCompany = 'Empresa '+JSON.parse(sessionStorage.user).data.id_empresa;
		};

		vm.getProfiles = function() {
			Api('profile/', 'GET').then((function(result) {
				switch (result.status) {
					case 200:

						vm.listProfiles = result.data.info;
						console.log("vm.listProfiles", vm.listProfiles);

						break;
					default:
						var _msg = 'Las credenciales de autenticación no se proporcionaron';
						Notify.send(_msg, {
							status: 'warning',
							timeout: 4500
						});
				}
			}), function(error) {
				switch (error) {
					case 400:
					case 401:
						var _msg = 'Las credenciales de autenticación no se proporcionaron';
						Notify.send(_msg, {
							status: 'warning',
							timeout: 4500
						});
						break;
					case 403:
						var _msg = 'No tiene permisos para consultar los Perfiles.';
						Notify.send(_msg, {
							status: 'warning',
							timeout: 4500
						});
						break;
					default:
						console.log("error inesperado. user_get");
				}
			});
		};

		vm.getGroups = function() {
			Api('group/', 'GET').then((function(result) {
				switch (result.status) {
					case 200:

						vm.listGroups = result.data.info;

						break;
					default:
						var _msg = 'Las credenciales de autenticación no se proporcionaron';
						Notify.send(_msg, {
							status: 'warning',
							timeout: 4500
						});
				}
			}), function(error) {
				switch (error) {
					case 400:
					case 401:
						var _msg = 'Las credenciales de autenticación no se proporcionaron';
						Notify.send(_msg, {
							status: 'warning',
							timeout: 4500
						});
						break;
					case 403:
						var _msg = 'No tiene permisos para consultar los Grupos.';
						Notify.send(_msg, {
							status: 'warning',
							timeout: 4500
						});
						break;
					default:
						console.log("error inesperado. user_get");
				}
			});
		};

		vm.fillFields = function(userId){

			vm.updateUserId = userId;

			vm.btnSHSave = false;
			vm.btnSHUpdate=true;
			vm.titleModal = "Infomación Usuario"
			vm.typeuser = true;
			vm.disabledCam = true;

			console.info('vm.listUsers*****', vm.listUsers);
			console.info('userId*****', userId);
			angular.forEach(vm.listUsers, function(value, index) {

				if (value.id == vm.updateUserId) {

					vm.clearForm();

					vm.obj = {
						first_name: value.first_name,
						last_name: value.last_name,
						username: value.username,
						password: value.password,
						email: value.email,
						telefono: parseInt(value.telefono),
						cedula: parseInt(value.cedula),
						id_perfil: value.perfil.id,
						id_empresa: value.empresa.id
					};
					console.info('vm.obj*****', vm.obj);
					$("#createUsers").modal();

					angular.forEach(vm.listGroups, function(value2, index2) {

						angular.forEach(value.grupos, function(value3,index3){

							if(value2.id == value3.id){
								document.getElementById("usersGroupsList")[index2].checked = true;
								console.log();
							}

						});

					});

					angular.forEach(vm.listCustomers, function(value2, index2) {

						angular.forEach(value.clientes, function(value3,index3){

							if(value2.id == value3.id){
								document.getElementById("usersCustomersList")[index2].checked = true;
								console.log();
							}

						});

					});

				}

			});
		};

		/**FIN***/

		vm.clearForm = function() {

			vm.obj = {
				first_name: '',
				last_name: '',
				username: '',
				password: '',
				passConfirm: '',
				email: '',
				telefono: '',
				cedula: '',
				id_empresa: null,
				id_perfil: null,
				ids_grupo: [],
				ids_cliente: []
			};

			if (vm.clearChecks) {

				angular.forEach(vm.listCustomers, function(value, index) {
					document.getElementById("usersCustomersList")[index].checked = false;
				});

				angular.forEach(vm.listGroups, function(value, index) {
					document.getElementById("usersGroupsList")[index].checked = false;
				});
			}
		};

	    vm.getListEvent = function (jsonData, jsonInfo) {
			Api('scheduler/list/', 'POST', jsonData).then((function(result) {
				$log.log('List createCalendar', result );
				switch (result.status) {
					case 200:
						$('#gifImage').html('');
						vm.datoList = result.data.info;
						var info;
						console.info('jQuery.isEmptyObject(result.data)',jQuery.isEmptyObject(result.data));
						if(jQuery.isEmptyObject(result.data)){
							info =result.data;
							jsonInfo.clientes = info;
						}else{
							info =result.data.info;
							//jsonInfo.clientes = info;
							var arrayInfoCliente =[];
							for(var i = 0; i<info.length;i++){
								var jsonInfoClent = {};
								var description = JSON.parse(info[i].description);

								jsonInfoClent.id = info[i].id;
								jsonInfoClent.nombre = description.nombre_cliente;
								jsonInfoClent.estado = 'no visitado';
								jsonInfoClent.coordenadas = [info[i].latitude,info[i].longitude];
								jsonInfoClent.date_prog = info[i].date_prog;
								// Realiza el Geo Inverso paar sacara la dirección
								vm.onGeoInverso(info[i], jsonInfoClent);

								arrayInfoCliente[i]=jsonInfoClent;
							}
							jsonInfo.clientes =arrayInfoCliente;
						}
						break;
					default:
						$log.error(result.status, result.message);
						var _msg='Ocurrio un error al intentar ';
						if (APP_CONFIG.DEBUG) {
							Notify.send(_msg, {
								status: 'warning',
								timeout: 7000
							});
						} else {
							Notify.send(_msg, {
								status: 'warning',
								timeout: 4000
							});
						}
				}
			}), function(error) {
				//$log.error('error get list description', error);
			});
		};

		vm.myDate2 = function () {

				var hoy = new Date();
				var dd = hoy.getDate();
				var mm = hoy.getMonth() + 1; //hoy es 0!
				var yyyy = hoy.getFullYear();

				if (dd < 10) {
					dd = '0' + dd;
				}

				if (mm < 10) {
					mm = '0' + mm;
				}


				return yyyy + '-' + mm + '-' + dd;
			};

		vm.myDateHours = function () {

			var hoy = new Date();
			var dd = hoy.getDate();
			var mm = hoy.getMonth() + 1; //hoy es 0!
			var yyyy = hoy.getFullYear();
			var h = hoy.getHours();
			var m = hoy.getMinutes() + 1; //hoy es 0!
			var s = hoy.getFullYear();


			if (dd < 10) {
				dd = '0' + dd;
			}

			if (mm < 10) {
				mm = '0' + mm;
			}

			if (h < 10) {
				h = '0' + h;
			}

			if (m < 10) {
				m = '0' + m;
			}

			if (s < 10) {
				s = '0' + s;
			}

				return yyyy + '-' + mm + '-' + dd +' '+h+':'+m+':'+':'+s;
			};

		vm.CalDate = function (date, type) {
			var fecha1 = moment(date, "YYYY-MM-DD HH:mm:ss");
			var fecha2 = moment(vm.myDateHours(), "YYYY-MM-DD HH:mm:ss");


			switch (type){
				case 'h':
					var diff = fecha2.diff(fecha1, 'h'); // Diff in hours
					break;
				case 'm':
					var diff = fecha2.diff(fecha1, 'm'); // Diff in minute
					break;
				case 's':
					var diff = fecha2.diff(fecha1, 's'); // Diff in seconds
					break;
			}

			 return diff;
		};

		vm.typemarker = function (date) {
			var time = vm.CalDate(date, 'm');
			var imgMarker = '';
			console.info('Tiempo', time);
			switch(true){
				case time>30:
					imgMarker = 'images/markers/circleYellow.png';
					break;
				case time<30:
					imgMarker = 'images/markers/circleGreen.png';
					break;
			}
			return imgMarker;
		}

			/**
			 * @author jorge Angarita
			 * @function GeoIvenrso apatir de latitude y longitude
			 *
			 */
		vm.onGeoInverso = function(json,jsonInfoClent) {

				console.log("onGeoInverso");

				var cx = json.latitude;
				var cy = json.longitude;
				if(cx === "0.0"){
					cx = "4.67248774"
				}

				if(cy === "0.0"){
					cy = "-74.06385402"
				}

				var objSend = { longitude: cy, latitude: cx };

				/*Notify.send("Buscando dirección...", {
					status: 'info',
					timeout: 3500
				});*/

				Anuket.moveMarkerCustomer = false;
				console.info('objSend', objSend);
				Api('sitidata/geoinverso/', 'POST', objSend).then((function(result) {
					console.info('result', result);
					switch (result.status) {
						case 200:
						case 201:
							if (result.data.latitude != 0 && result.data.longitude != 0) {
								/*Notify.send("Geo localización exitosa..", {
									status: 'success',
									timeout: 3500
								});*/

								jsonInfoClent.direccion = result.data.address1;
							} else {
								vm.campos.cx = '';
								vm.campos.cy = '';
								Notify.send("Error al buscar la dirección. Por favor ubique el punto de forma manual.", {
									status: 'warning',
									timeout: 6000
								});
							}

							break;
						default:
							var _msg = 'Las credenciales de autenticación no se proporcionaron';
							Notify.send(_msg, {
								status: 'warning',
								timeout: 4500
							});
					}
				}), function(error) {
					switch (error) {
						case 400:
						case 401:
							var _msg = 'Las credenciales de autenticación no se proporcionaron';
							Notify.send(_msg, {
								status: 'warning',
								timeout: 4500
							});
							break;
						case 403:
							var _msg = 'No tiene permisos para consultar el Geo.';
							Notify.send(_msg, {
								status: 'warning',
								timeout: 4500
							});
							break;
						default:
							console.log("error inesperado. user_get");
					}
				});

			};

        vm.fillData = function() {
			vm.getUsers();;
			vm.getCompany();
			vm.getProfiles();
			vm.getCustomers();
			vm.getGroups();
            angular.forEach(vm.listCustomerMap, function(value, index) {
                var suma = value.visitados + value.novisitados;
                var total = (value.visitados * 100) / suma;
                vm.listCustomerMap[index].porcentaje = Math.round(total);

                var bateria = ['success', 'warning', 'danger'];


                if (parseInt(value.bateria) >= 70 && parseInt(value.bateria) <= 100) {
                    vm.listCustomerMap[index].colorBateria = bateria[0];
                } else if (parseInt(value.bateria) >= 30 && parseInt(value.bateria) <= 69) {
                    vm.listCustomerMap[index].colorBateria = bateria[1];
                } else {
                    vm.listCustomerMap[index].colorBateria = bateria[2];
                }

            });

            console.log(vm.listCustomerMap)
        };

        vm.toggleSidebar = function() {

            vm.toggleSideBar = !vm.toggleSideBar;
            vm.toggleArrow = !vm.toggleArrow;
            vm.textArrow = !vm.textArrow;
        }

        vm.toggleSidebar2 = function() {

            vm.toggleSideBar2 = !vm.toggleSideBar2;
            vm.toggleArrow2 = !vm.toggleArrow2;
            vm.textArrow2 = !vm.textArrow2;
        }

        vm.toggleRoute = function(option, idContent) {

            vm["customersVN" + idContent] = false;
            vm["routeUserCurrent" + idContent] = false;
            vm["arrowHideCR" + idContent] = "▲";

            document.getElementById('mapRoute' + idContent).classList.remove('heightNone');

            if (option === 'customers') {
                vm["customersVN" + idContent] = true;
                vm.addMarker('allCustomer', idContent);
            }

            if (option === 'route') {
                vm["routeUserCurrent" + idContent] = true;
                vm.addMarker('allRoute', idContent);
            }

            if (option === 'hide') {
                document.getElementById('mapRoute' + idContent).classList.add('heightNone');
                vm["arrowHideCR" + idContent] = "";
            }

			if (option === 'conocimiento') {
				vm["customersVN" + idContent] = true;
			}

			if (option === 'herramineta') {
				vm["routeUserCurrent" + idContent] = true;
			}
        };

        vm.centerMap = function(lat, lng) {
            Anuket.centerMap(lat, lng);
            Anuket.setZoom(14)
        };

        vm.infoMarker = {
            'name': 'Servinformación',
            'address': 'Calle 84 24-78',
            'city': 'Bogotá'
        };

        vm.car_route = function() {
            $log.info('draw route using car_route');
        };

        vm.walk_route = function() {
            $log.info('draw route using walk_route');
        };

        vm.subway_route = function() {
            $log.info('draw route using subway_route');
        };

        vm.markerClick = function(e, coords) {
            $("#modalPan").on("shown.bs.modal", function(e) {
                Anuket.streetView(coords);
            });

            $('#modalPan').modal('show');
        };

        vm.initGMaps = function() {
            $log.info('initialize Map');

            Anuket.run('#themap', {
                'zoom': currentZoom,
                'latlng': currentPos,
                'initMarker': true,
                'markerClick': vm.markerClick
            });
            Anuket.setMapType('night_mode');
        };

        vm.usersPositions = function() {
            refUsers.on("value", function(snap) {

                var datos = snap.val();

                angular.forEach(datos, function(value, index) {

                    Anuket.addMarker({ "lat": value.lat, "lng": value.lng }, false, '', null, null, 'images/markers/circleBlue.png', value.id.toString());

                })

            });
        }

        vm.updateUsers = function() {

            //Anuket.removeMarkers();

            var userToUpdate = refUsers.child('usuario1');

            userToUpdate.once("value", function(snap) {

                var datos = snap.val();

                var lngNew = datos.lng + 0.000023499999998932708;

                userToUpdate.update({
                    id: 1,
                    lat: datos.lat,
                    lng: lngNew
                        //lat:4.751966,
                        //lng:-74.116602
                });

            });

        }

        vm.goCalendar = function(id, nameuser,identificationcard) {

				var obj = JSON.stringify({"application_id": 1,"status_id": 1,"user_id": id,"nameuser": nameuser,"identificationcard": identificationcard});
				sessionStorage.setItem("userCalendar",obj);

				$state.go("app.calendar");

			};

        vm.main = function() {

            refUsers = firebase.database().ref().child("usuarios");

            if (sessionStorage.session != undefined) {
				APP_CONFIG.AUTH2_TOKEN = JSON.parse(sessionStorage.user).data.token.access_token;
                vm.initGMaps();
                vm.fillData();
                vm.toggleSidebar();
                vm.alarmType();
                vm.usersPositions();


                setTimeout(function() {

                    $('#mapCustomerTable').DataTable({
                        "language": {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                        },
                        "searching": true,
                        "paging": false,
                        "info": false,
                        "scrollX": false,
                        "ordering": false
                    });

					/*$('#mapCustomerTabletarea').DataTable({
						"language": {
							"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
						},
						"searching": true,
						"paging": false,
						"info": false,
						"scrollX": false,
						"ordering": false
					});*/

                    $('#mapAlertsTable').DataTable({
                        "language": {
                            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                        },
                        "searching": true,
                        "paging": false,
                        "info": false,
                        "scrollX": false,
                        "ordering": false
                    });

                }, 1000);

            } else {
                $state.go('app.slogin');
            };

        };

        vm.main();
    } //baseMapController

    angular.module('app.core').controller('baseMapController', baseMapController); baseMapController.$inject = [
		'$scope',
		'$timeout',
		'$log',
		'Api',
		'Notify',
		'APP_CONFIG',
		'$state'
    ];

})();
