/**
 * Created by JANGARITA on 28/11/2017.
 */
(function() {
	'use strict';

	function functionController($scope, $state, $timeout, $log, Api, Notify, Alerts, APP_CONFIG) {
		var vm = this;
		vm.campos = {};
		var sessionStorageValue = JSON.parse(sessionStorage.userCalendar);
		vm.application_id = sessionStorageValue.application_id;

		/****/
		vm.tipo = 'A';
		vm.directionsgeo = false;
		vm.chackrepeti = true;
		vm.campos.direccion = '';
		vm.campos.cliente = '';
		vm.campos.event = '';
		vm.campos.cx = '';
		vm.campos.cy = '';
		vm.campos.finicre = '';
		vm.campos.checkboxselect = false;
		vm.campos.rephasta = false;
		vm.rep = false;
		vm.campos.rango = '';
		vm.btnSHSave= true;
		vm.btnSHDelete= false;
		vm.btnSHUpdate= false;
		vm.btnSHUpdate=false;
		vm.btnSHDelete=false;


		vm.prueba = function () {
			vm.campos.prueba = vm.ucWords("hola");
		}

		vm.listaClientforUsers = function (iduser) {
			Api('user/'+vm.user_id, 'GET').then((function(result) {
				switch (result.status) {
					case 200:

						console.info('result.data.info',result.data);
						vm.campos.listCliente =  result.data.clientes;
						console.info('vm.campos.listCliente',vm.campos.listCliente);

						break;
					default:
						var _msg = 'Las credenciales de autenticación no se proporcionaron';
						Notify.send(_msg, {
							status: 'warning',
							timeout: 4500
						});
				}
			}), function(error) {
				switch (error) {
					case 400:
					case 401:
						var _msg = 'Las credenciales de autenticación no se proporcionaron';
						Notify.send(_msg, {
							status: 'warning',
							timeout: 4500
						});
						break;
					default:
						console.log("error inesperado. user_get");
				}
			});

		}

		/**
		 * @author jorge Angarita
		 * @function Colocar la primera letra de cada palabra en mayuscula
		 *
		 */
		vm.ucWords= function (string){
			var arrayWords;
			var returnString = "";
			var len;
			console.info('ucWords',string);
			arrayWords = string.split(" ");
			len = arrayWords.length;
			for(var i=0;i < len ;i++){
				if(i != (len-1)){
					returnString = returnString+vm.ucFirst(arrayWords[i])+" ";
				}
				else{
					returnString = returnString+vm.ucFirst(arrayWords[i]);
				}
			}
			return returnString;
		}
		/**
		 * @author jorge Angarita
		 * @function Segunda para de Colocar la primera letra de cada palabra en mayuscula
		 *
		 */
		vm.ucFirst= function(string){
			return string.substr(0,1).toUpperCase()+string.substr(1,string.length).toLowerCase();
		}
		/**
		 * @author jorge Angarita
		 * @function agendamientoMedico/SchedulerConfig/
		 *
		 */
		vm.listdes = function () {

			Api(vm.urlbase[tyoeUrl]+'agendamientoMedico/SchedulerConfig/'+vm.application_id, 'GET', null, {'external': true}).then((function(result) {
				//Api('agendamientoMedico/SchedulerConfig/'+vm.application_id, 'GET').then((function(result) {
				$log.log('Api.get Descripcuón ******************');
				switch (result.status) {
					case 200:
						//$log.error('Lista descripción', result.data);
						var dataList = result.data;
						var arraydesp = [];
						console.info('++++dataList++++', dataList);
						for(var ind=0; ind<dataList.length; ind++){
							var objDesp = {};
							objDesp.id = dataList[ind].id;
							objDesp.name = dataList[ind].name;
						}
						vm.listaDesciones = arraydesp;
						$log.info('Lista descripción****', vm.listaDesciones);

						break;
					default:
						$log.error(result.status, result.message);
						var _msg='Ocurrio un error al intentar mostar la lista de descripciones';
						if (APP_CONFIG.DEBUG) {
							Notify.send(_msg, {
								status: 'warning',
								timeout: 7000
							});
						} else {
							Notify.send(_msg, {
								status: 'warning',
								timeout: 4000
							});
						}
				}
			}), function(error) {
				$log.error('error get login', error);
			});
		};
		/**
		 * @author jorge Angarita
		 * @function Lista los tipo de eventos por medico, Ppara tablas y combos
		 *
		 */
		vm.ListTypeEvent = function () {
			$log.log('*****LIATA TIPO DE EVENTO********');
			Api('activity/', 'GET', null).then((function(result) {
				//Api('agendamientoMedico/ListTypeAppointmentId/'+vm.session_id, 'GET', null).then((function (result) {
				//$log.log('LIATA TIPO DE EVENTO', result);
				switch (result.status) {
					case 200:
						$log.log('*****result.data********', result.data);;
						vm.resultListTypeEvent = result.data;
						var dataList = result.data.info;
						var arraydesp = [];
						//$log.log('*****dataList********', dataList);
						for(var ind=0; ind<dataList.length; ind++){
							var objDesp = {};
							objDesp.id = dataList[ind].id;
							objDesp.nombre_actividad = dataList[ind].nombre_actividad;
							objDesp.tiempo = dataList[ind].tiempo;
							arraydesp[ind] = objDesp;
						}
						vm.listaEvent = arraydesp;
						console.info('vm.listaEvent', vm.listaEvent)

						break;
					default:
						$log.error(result.status, result.message);
						var _msg = 'Ocurrio un error al intentar crear el evento de agenda';
						if (APP_CONFIG.DEBUG) {
							Notify.send(_msg, {
								status: 'warning',
								timeout: 7000
							});
						} else {$log.info('Lista descripción', vm.listaDesciones);
							Notify.send(_msg, {
								status: 'warning',
								timeout: 4000
							});
						}
				}
			}), function (error) {
				$log.error('error get login', error);
			});
		}

		/**
		 * @author jorge Angarita
		 * @function Lista de horas por el ranga dinamico
		 *
		 */
		vm.TimeListD = function (mun) {

			var listDatos = [];
			var ini =  0;
			// fecha inicial
			var d = new Date();
			d.setHours(1)
			d.setMinutes(0);
			d.setSeconds(0)
			//$log.info('***** listDatos d ****', d);
			var fini = moment(d).format('HH:mm:ss');
			// fecha final
			var f = new Date();
			f.setHours(23)
			f.setMinutes(0);
			f.setSeconds(0)
			//$log.info('***** listDatos f ****', f);
			var ffin = moment(f).format('HH:mm:ss');
			/*$log.info('***** Hora Inicio ****', moment(d).format('HH:mm:ss'));
			 $log.info('***** Hora Final ****', ffin);
			 $log.info('***** Minutos a sumar ****', mun);*/
			listDatos[ini] = fini;

			do{
				ini++;
				d.setMinutes(d.getMinutes() + mun);
				listDatos[ini] = moment(d).format('HH:mm:ss');
			}while(listDatos[ini] < ffin);

			return listDatos;
		};
		/**
		 * @author jorge Angarita
		 * @function Formato de fecha con la función Date()
		 *
		 */
		vm.myDate = function () {

			var hoy = new Date();
			var dd = hoy.getDate();
			var mm = hoy.getMonth() + 1; //hoy es 0!
			var yyyy = hoy.getFullYear();

			if (dd < 10) {
				dd = '0' + dd;
			}

			if (mm < 10) {
				mm = '0' + mm;
			}

			return dd + '/' + mm + '/' + yyyy;
		};

		vm.myDate2 = function () {

			var hoy = new Date();
			var dd = hoy.getDate();
			var mm = hoy.getMonth() + 1; //hoy es 0!
			var yyyy = hoy.getFullYear();

			if (dd < 10) {
				dd = '0' + dd;
			}

			if (mm < 10) {
				mm = '0' + mm;
			}


			return yyyy + '-' + mm + '-' + dd;
		};
		/**
		 * @author jorge Angarita
		 * @function Colocar la primera letra de cada palabra en mayuscula
		 *
		 */
		vm.ucWords= function (string){
			var arrayWords;
			var returnString = "";
			var len;
			console.info('ucWords',string);
			arrayWords = string.split(" ");
			len = arrayWords.length;
			for(var i=0;i < len ;i++){
				if(i != (len-1)){
					returnString = returnString+vm.ucFirst(arrayWords[i])+" ";
				}
				else{
					returnString = returnString+vm.ucFirst(arrayWords[i]);
				}
			}
			return returnString;
		}
		/**
		 * @author jorge Angarita
		 * @function Segunda para de Colocar la primera letra de cada palabra en mayuscula
		 *
		 */
		vm.ucFirst= function(string){
			return string.substr(0,1).toUpperCase()+string.substr(1,string.length).toLowerCase();
		}
		/**
		 * @author jorge Angarita
		 * @function carga las funciones del calendario
		 *
		 */
		vm.loadDatePicker = function(){
			$log.info('loading loadDatePicker');

			var $datepicker = $('.datepicker');

			$datepicker.datetimepicker({
				format: 'DD/MM/YYYY',
				icons: {
					time: "fa fa-clock-o",
					date: "fa fa-calendar",
					up: "fa fa-chevron-up",
					down: "fa fa-chevron-down",
					previous: 'fa fa-chevron-left',
					next: 'fa fa-chevron-right',
					today: 'fa fa-screenshot',
					clear: 'fa fa-trash',
					close: 'fa fa-remove',
					inline: true
				}
			});
		};
		/**
		 * @author jorge Angarita
		 * @function Activar el Mapa, se debe colocar en el metodo donde se valla amostrar el Mapa
		 *
		 */
		vm.loadMap = function() {
			$("#createCustomer2").on("shown.bs.modal", function(e) {
				//Anuket.streetView(coords);
				Anuket.run('#mapCustomer2', {
					'zoom': 12,
					'latlng': { "lat": 4.6725634, "lng": -74.064028 },
					'initMarker': false
				});

				Anuket.removeMarkers();
				Anuket.addMarker({ "lat": parseFloat(4.6725634), "lng": parseFloat(-74.064028) }, true, 'createCustomer2', null);
			});
		}

		/**
		 * @author jorge Angarita
		 * @function Georeferencia la dirección y muesta latitude y longitude
		 *
		 */
		vm.onBlur = function() {
			/**
			 * A => Eventos que reconocen la variable global
			 * B => Eventos qu eno reconocen las variable y toca por Jquery
			 * **/
			console.log("onblur");
			console.info('vm.campos***', vm.campos);
			var addressCustomer = vm.campos.direccion.split(',');
			//cll 84 24 78
			console.info('addressCustomer', addressCustomer);
			if (addressCustomer.length > 1) {

				var objSend = { city: addressCustomer[1], address: addressCustomer[0] };

				Notify.send("Buscando dirección...", {
					status: 'info',
					timeout: 3500
				});

				Anuket.moveMarkerCustomer = false;
				console.info('objSend', objSend);
				Api('sitidata/geocoder/', 'POST', objSend).then((function(result) {
					console.info('result onBlur', result);
					switch (result.status) {
						case 200:
						case 201:
							if (result.data.latitude != 0 && result.data.longitude != 0) {

								Notify.send("Geo localización exitosa..", {
									status: 'success',
									timeout: 3500
								});
								vm.campos.cx = result.data.latitude;
								$('.selectpicker').selectpicker('refresh');
								vm.campos.cy = result.data.longitude;
								$('.selectpicker').selectpicker('refresh');
								vm.directionsgeo = true;
								$('.selectpicker').selectpicker('refresh');
								/*$('#addressCustomerGeo').removeClass( "ng-hide" );
								 $('.selectpicker').selectpicker('refresh');*/
								vm.campos.direccionGeo = result.data.dirtrad;
								$('.selectpicker').selectpicker('refresh');
								console.info('vm.obj.cx', vm.campos.cx);
								console.info('vm.obj.cx', vm.campos.cy);
								if(vm.tipo === 'B'){
									$('#directionsgeo').removeClass( "ng-hide" );
									$('.selectpicker').selectpicker('refresh');
									$('#direccionGeo').val(result.data.dirtrad);
									$('.selectpicker').selectpicker('refresh');
									$('#cxCustomer').val(vm.campos.cx);
									$('.selectpicker').selectpicker('refresh');
									$('#cyCustomer').val(vm.campos.cy);
									$('.selectpicker').selectpicker('refresh');
								}

								Anuket.removeMarkers();
								Anuket.addMarker({ "lat": parseFloat(result.data.latitude), "lng": parseFloat(result.data.longitude) }, true, 'createCustomer2', null);
								Anuket.centerMap(parseFloat(result.data.latitude),parseFloat(result.data.longitude));

							} else {
								vm.campos.cx = '';
								vm.campos.cy = '';
								Notify.send("Error al buscar la dirección. Por favor ubique el punto de forma manual.", {
									status: 'warning',
									timeout: 6000
								});
							}

							break;
						default:
							var _msg = 'Las credenciales de autenticación no se proporcionaron';
							Notify.send(_msg, {
								status: 'warning',
								timeout: 4500
							});
					}
				}), function(error) {
					switch (error) {
						case 400:
						case 401:
							var _msg = 'Las credenciales de autenticación no se proporcionaron';
							Notify.send(_msg, {
								status: 'warning',
								timeout: 4500
							});
							break;
						case 403:
							var _msg = 'No tiene permisos para consultar el Geo.';
							Notify.send(_msg, {
								status: 'warning',
								timeout: 4500
							});
							break;
						default:
							console.log("error inesperado. user_get");
					}
				});

			} else {
				Notify.send("La dirección no contiene el siguiente formato “dirección, ciudad” ", {
					status: 'warning',
					timeout: 7000
				});
			}
		};
		/**
		 * @author jorge Angarita
		 * @function GeoIvenrso a paratir de latitude y longitude
		 *
		 */
		vm.onGeoInverso = function() {

			console.log("onGeoInverso");

			var cx = vm.campos.cx;
			var cy = vm.campos.cy;
			if(cx === "0.0"){
				cx = "4.67248774"
			}

			if(cy === "0.0"){
				cy = "-74.06385402"
			}
			//cll 84 24 78
			console.info('addressCustomer', addressCustomer);


			var objSend = { longitude: cy, latitude: cx };

			Notify.send("Buscando dirección...", {
				status: 'info',
				timeout: 3500
			});

			Anuket.moveMarkerCustomer = false;
			console.info('objSend', objSend);
			Api('sitidata/geoinverso/', 'POST', objSend).then((function(result) {
				console.info('result', result);
				switch (result.status) {
					case 200:
					case 201:
						if (result.data.latitude != 0 && result.data.longitude != 0) {

							Notify.send("Geo localización exitosa..", {
								status: 'success',
								timeout: 3500
							});
							vm.campos.cx = cx;
							vm.campos.cy = cy;
							vm.campos.direccion = result.data.address1+","+result.data.city1;

							Anuket.removeMarkers();
							Anuket.addMarker({ "lat": parseFloat(cx), "lng": parseFloat(cy) }, true, 'createCustomer2', null);
							Anuket.centerMap(parseFloat(cx),parseFloat(cy));

						} else {
							vm.campos.cx = '';
							vm.campos.cy = '';
							Notify.send("Error al buscar la dirección. Por favor ubique el punto de forma manual.", {
								status: 'warning',
								timeout: 6000
							});
						}

						break;
					default:
						var _msg = 'Las credenciales de autenticación no se proporcionaron';
						Notify.send(_msg, {
							status: 'warning',
							timeout: 4500
						});
				}
			}), function(error) {
				switch (error) {
					case 400:
					case 401:
						var _msg = 'Las credenciales de autenticación no se proporcionaron';
						Notify.send(_msg, {
							status: 'warning',
							timeout: 4500
						});
						break;
					case 403:
						var _msg = 'No tiene permisos para consultar el Geo.';
						Notify.send(_msg, {
							status: 'warning',
							timeout: 4500
						});
						break;
					default:
						console.log("error inesperado. user_get");
				}
			});
		};

		/**
		 * @author jorge Angarita
		 * @function abre el modeal #myModalTyeEvent
		 *
		 */
		vm.envenInfoTypeEvent = function (event) {
			$log.info('envenInfoTypeEvent',event);
			var odjJson = JSON.parse(event);
			vm.campos.rango = '';
			vm.campos.rango = vm.TimeListD(odjJson.tiempo);

			setTimeout(function(){
				$('.selectpicker').selectpicker('refresh');
			},100);
			$log.info('vm.campos.rangoHora', vm.campos.rangoHora);


		}

		/**
		 * @author jorge Angarita
		 * @function valida los campos del formulario, antes de crear el evento
		 *
		 */
		vm.validator = function () {
			var _msg = "";
			var val = false;
			console.info('vm.campos', vm.campos);
			if(vm.campos.direccion === '' || vm.campos.direccion === undefined){
				console.info('direccion');
				_msg = "El campo dirección es obligatorio";
				val = true;
			}else{
				val = false;
			}
			if(vm.campos.cliente === '' || vm.campos.cliente === undefined){
				console.info('cliente');
				_msg += "<br/> El campo cliente es obligatorio";
				val = true;
			}else{
				val = false;
			}
			if(vm.campos.finicre === '' || vm.campos.finicre === undefined){
				console.info('finicre');
				_msg += "<br/> El campo fecha es obligatorio";
				val = true;
			}else{
				val = false;
			}
			if(vm.campos.event === '' || vm.campos.event === undefined){
				console.info('event');
				_msg += "<br/> El campo Tipo de Evento es obligatorio";
				val = true;
			}else{
				val = false;
			}
			if(vm.campos.event !== ''){
				if(vm.campos.hini.trim()> vm.campos.hfin.trim()){
					console.info('hini');
					_msg += "La hola fin no puede ser menor a la fecha inicial";
					val = true;
				}else{
					val = false;
				}
			}


			if(vm.campos.checkboxselect === true){
				if(vm.campos.selectrep === '' || vm.campos.selectrep === undefined){
					_msg += "<br/> El campo Tipo de repetición es obligatorio";
					val = true;
				}else{
					val = false;
				}
			}


			vm.campos.validatorcam = val;
			if(val){
				Notify.send(_msg, {
					status: 'warning',
					timeout: 4000
				});
			}


		}

		vm.validatorUpdate = function () {
			var _msg = "";
			var val = false;
			console.info('vm.campos', vm.campos);
			if($("#direccion").val() === '' || $("#direccion").val() === undefined){
				console.info('direccion');
				_msg = "El campo dirección es obligatorio";
				val = true;
			}else{
				val = false;
			}
			if($("#cliente").val() === '' || $("#cliente").val() === undefined){
				console.info('cliente');
				_msg += "<br/> El campo cliente es obligatorio";
				val = true;
			}else{
				val = false;
			}
			if($("#finifinicre").val() === '' || $("#finifinicre").val() === undefined){
				console.info('finicre');
				_msg += "<br/> El campo fecha es obligatorio";
				val = true;
			}else{
				val = false;
			}
			if($("#event").val() === '' || $("#event").val() === undefined){
				console.info('event');
				_msg += "<br/> El campo Tipo de Evento es obligatorio";
				val = true;
			}else{
				val = false;
			}
			if($("#event").val() !== ''){
				if($("#hini").val().trim()> $("#hfin").val().trim()){
					console.info('hini');
					_msg += "La hola fin no puede ser menor a la fecha inicial";
					val = true;
				}else{
					val = false;
				}
			}


			/*if(vm.campos.checkboxselect === true){
			 if(vm.campos.selectrep === '' || vm.campos.selectrep === undefined){
			 _msg += "<br/> El campo Tipo de repetición es obligatorio";
			 val = true;
			 }else{
			 val = false;
			 }
			 }*/


			vm.campos.validatorcam = val;
			if(val){
				Notify.send(_msg, {
					status: 'warning',
					timeout: 4000
				});
			}


		}

		vm.validatordata = function (tipo) {
			console.info('validatordata');
			console.info(vm.campos.hini.trim(), vm.campos.hfin.trim());
			var _msg = "";
			var val = false;

			switch (tipo.trim()){
				case 'F':
					if(vm.campos.finicre.trim()> vm.campos.fecharango.trim()){
						_msg = "La fecha de repeteición  no puede ser menor a la fecha inicial";
						val = true;
					}else{
						val = false;
					}
					break;
				case 'H':
					if(vm.campos.hini.trim()> vm.campos.hfin.trim()){
						console.info('SI');
						_msg = "La hola fin no puede ser menor a la hora inicial";
						val = true;
					}else{
						console.info('NO');
						val = false;
					}
					break;
			}

			console.info(val, ' FALSE');
			if(val){
				console.info(val, ' TRUE');
				Notify.send(_msg, {
					status: 'warning',
					timeout: 4000
				});
			}
		}


		vm.confirmTarea = function(option, userId) {
			console.log("confirm",option);
			switch (option) {
				case 'save':
					vm.campos.textConfirmUsers = 'Se puese sobrelapar si existe otro evento con la misma franja horaria.';
					vm.validatorTarea();
					break;
				case 'delete':
					vm.campos.validatorcam =false;
					vm.campos.textConfirmUsers = 'Esta seguro de eliminar este evento.';
					vm.campos.deleteUserId = userId;
					break;
				case 'update':
					vm.campos.textConfirmUsers = 'Esta seguro de actualizar este usuario.';
					vm.validatorUpdate();
					break;
				default:
					console.log("error option incorrect users");
			}

			vm.campos.selectOption = option;
			console.log("vm.campos.selectOption",vm.campos.selectOption);

			if(vm.campos.validatorcam === false){
				$("#confirmUsers").modal();
			}

		};

		vm.validatorTarea = function () {
			var _msg = "";
			var val = false;
			console.info('vm.campos', vm.campos);
			if(vm.campos.direccion === '' || vm.campos.direccion === undefined){
				console.info('direccion');
				_msg = "El campo dirección es obligatorio";
				val = true;
			}else{
				val = false;
			}
			if(vm.campos.cliente === '' || vm.campos.cliente === undefined){
				console.info('cliente');
				_msg += "<br/> El campo cliente es obligatorio";
				val = true;
			}else{
				val = false;
			}
			if(vm.campos.finicre === '' || vm.campos.finicre === undefined){
				console.info('finicre');
				_msg += "<br/> El campo fecha es obligatorio";
				val = true;
			}else{
				val = false;
			}
			if(vm.campos.event === '' || vm.campos.event === undefined){
				console.info('event');
				_msg += "<br/> El campo Tipo de Evento es obligatorio";
				val = true;
			}else{
				val = false;
			}
			if(vm.campos.event !== ''){
				if(vm.campos.hini.trim()> vm.campos.hfin.trim()){
					console.info('hini');
					_msg += "La hola fin no puede ser menor a la fecha inicial";
					val = true;
				}else{
					val = false;
				}
			}


			if(vm.campos.checkboxselect === true){
				if(vm.campos.selectrep === '' || vm.campos.selectrep === undefined){
					_msg += "<br/> El campo Tipo de repetición es obligatorio";
					val = true;
				}else{
					val = false;
				}
			}


			vm.campos.validatorcam = val;
			if(val){
				Notify.send(_msg, {
					status: 'warning',
					timeout: 4000
				});
			}


		}

		vm.selectActionTarea = function () {
			$scope.formData = {};
			console.info('selectAction', vm.campos.selectOption)
			switch (vm.campos.selectOption){
				case 'save':
					vm.docrearAgendaTarea('A');
					break;
				case 'delete':
					vm.doeliminarAgenda();
					break;
				case 'update':
					vm.doreprogramarAgenda('R');
					break;
			}
		}

		/**
		 * @author jorge Angarita
		 * @function Guarda el agendamiento por parte d elos dostores  para los eventos privados o publicos (ghost)
		 *
		 */
		vm.docrearAgendaTarea = function (tipo) {
			$log.info('loading calendar****', tipo);

			var fini;
			var ffin;
			var metodo;
			var jsonDataIn = {};
			console.info('docrearAgenda vm.campos tarea ', vm.campos);

			/***
			 * Eventos para el calendario
			 * A => Agenda progranada
			 * G => Tiempo disponible
			 * GA => Tiempo disponible Actualizdo
			 * AD => Agenda Eliminada
			 * GD => Tiempo libre eliminado
			 ***/
			var objUser = JSON.parse(vm.campos.user);
			switch (tipo){
				case 'A':
					fini =  moment($('#finifinicre').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
					metodo = 'POST';

					var objClient = JSON.parse(vm.campos.cliente);
					var objType_event = JSON.parse(vm.campos.event);
					var objInfoData = {};
					objInfoData.id_cliente = objClient.id;
					objInfoData.nombre_cliente = objClient.nombre_cliente;
					objInfoData.id_actividad = objType_event.id;
					objInfoData.nombre_actividad = objType_event.nombre_actividad;
					objInfoData.tiempo_actividad = objType_event.tiempo;
					console.info('client', objClient);
					console.info('type_event', objType_event);
					console.info('infoData', objInfoData);
					jsonDataIn.init_date = fini;
					jsonDataIn.init_hour = vm.campos.hini;
					jsonDataIn.end_hour = vm.campos.hfin;
					jsonDataIn.latitude = vm.campos.cx,
						jsonDataIn.longitude = vm.campos.cy,
						jsonDataIn.ghost = false;
					jsonDataIn.description = JSON.stringify(objInfoData);
					jsonDataIn.description_id = 99999;
					break
				case 'G':
					fini =  moment($('#finig').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
					ffin = moment($('#ffing').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
					metodo = 'POST';
					jsonDataIn.init_date = fini;
					jsonDataIn.end_date = ffin;
					jsonDataIn.init_hour = vm.campos.hini;
					jsonDataIn.end_hour = vm.campos.hfin;
					jsonDataIn.ghost = true;

					var obJson = JSON.parse(vm.campos.descrip);
					jsonDataIn.description = obJson.name;
					jsonDataIn.description_id = obJson.id;
					break;
				case 'GA':
					metodo = 'POST';
					vm.campos.checkboxselect = false;
					break;
				case 'AD':
					break;
				case 'GD':
					break;
				default:
					break;
			}

			var val =  false;
			var fvalue;
			var dd = new  Date();
			var day = moment().format('YYYY-MM-DD');
			$log.info('Fecha actual', day);
			$log.info('jsonDataIn', jsonDataIn);
			if(fini >= day){
				$log.info('***fini >= day****');
				vm.MgsFini = false;
				//Validación Fechas
				//$log.info('***jsonDataIn.repeat = "NONE"****');
				jsonDataIn.repeat = "NONE";
				jsonDataIn.end_date = fini;
				//Fin Validación Fechas

				//Validación campo hora
				if (vm.campos.hini === undefined || vm.campos.hini === null) {
					vm.MgsHini = true;
					vm.MsgMgsHini = "Debe seleccionar una hora";
					return false;
				} else if (vm.campos.hini === undefined || vm.campos.hini === null) {
					vm.MgsHfin = true;
					vm.FechaHfin = "Debe seleccionar una hora";
				} else {
					vm.MgsHini = false;
					vm.MgsHfin = false;
				}


				var jsonData = {};
				jsonData.user_id = objUser;
				jsonData.client_id = objInfoData.id_cliente;
				jsonData.timezones = [jsonDataIn];


				if (val === false) {
					$log.info('***Info jsonData****', jsonData);
					Api('events/', 'POST', jsonData).then((function (result) {
						$log.log('Api.post CRear Agenda', result);
						switch (result.status) {
							case 201:

								$('#createCustomer2').modal('hide');
								$('#confirmUsers').modal('hide');
								vm.rep = false;
								vm.campos.rephasta = false;
								vm.campos.checkboxselect = false;
								vm.rep = false;
								vm.campos.rephasta =false;
								break;
							default:
								$log.error(result.status, result.message);
								var _msg = 'Ocurrio un error al intentar crear el evento de agenda';
								if (APP_CONFIG.DEBUG) {
									Notify.send(_msg, {
										status: 'warning',
										timeout: 7000
									});
								} else {
									Notify.send(_msg, {
										status: 'warning',
										timeout: 4000
									});
								}
						}
					}), function (error) {
						$log.error('error get events/create', error);
					});
				}
			}else{
				vm.desMsgFechaIni = "Fecha inicial  debe ser menor ó igual a la fecha actual";
				vm.MgsFini = true;
				return false;
			}
		};

		vm.session = function () {

			vm.getAll();
		};

		vm.getAll = function () {
			$log.info('userCalendar', JSON.parse(sessionStorage.userCalendar));
			vm.prueba();
			vm.user_id = sessionStorageValue.user_id;
			$log.info('vm.user_id', vm.user_id);
			vm.listaClientforUsers(1);
			vm.ListTypeEvent();
			vm.loadDatePicker();
			console.info('vm.selectOption',vm.selectOption);


			//$log.info(vm.TimeListD());
		}

		vm.main = function() {
			//setTimeout("location.reload()", 5000);
			if (sessionStorage.session != undefined) {
				APP_CONFIG.AUTH2_TOKEN = JSON.parse(sessionStorage.user).data.token.access_token;
				vm.session();
			} else {

				$state.go('app.slogin');
			};

		};

		vm.main();
	} //calendarController

	angular.module('app.core').controller('functionController', functionController);
	functionController.$inject = [
		'$scope',
		'$state',
		'$timeout',
		'$log',
		'Api',
		'Notify',
		'Alerts',
		'APP_CONFIG'
	];
})();
