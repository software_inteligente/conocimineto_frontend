(function() {
    'use strict';

    function calendarController($scope, $cacheFactory, $state, $timeout, $log, Api, Notify, Alerts, APP_CONFIG) {
        var vm = this;
        vm.campos = {};
		vm.urlbase = [];
		Anuket.moveMarkerCustomer = false;
		var tyoeUrl = 'cloud-rutakontrol';
		vm.urlbase['local']= "http://localhost:80/proyectos/BancoPopular/BancoPopular_BackEnd/public/api/";
		vm.urlbase['appengine']= "http://servicesadmin1.banco-popular-183821.appspot.com/api/";
		vm.urlbase['appengine_sitidoctor']= "http://servicesadmin.praxis-medic.appspot.com/api/";
		vm.urlbase['cloud-rutakontrol']= "https://backend.cloud-rutakontrol.appspot.com/";
        vm.rep = false;
		vm.campos.rephasta =false;
		vm.chackrepeti = true;
		vm.directionsgeo = false;
		vm.datoList ={};
		var sessionStorageValue = JSON.parse(sessionStorage.userCalendar);
		vm.application_id = sessionStorageValue.application_id;
		//vm.session_id = localStorage.getItem('id_user');
		vm.session_id = 21;
		vm.MgsFfin = false;
		vm.MgsFini = false;
		vm.MgsIni = false;
		vm.campos.hinighost = '00:00:00';
		vm.campos.hfinghost = '00:00:00';
		vm.repetition = [];
		vm.typeuser = sessionStorage.userCalendar;
		vm.repetition['ALLDAY'] = 'todo el dia';
		vm.repetition['DAY'] = '<strong>Cada día</strong>,este evento se repite cada día durante el tiempo estipulado en rango de fechas ó si no se toma 30 días por defecto';
		vm.repetition['M|TU|W|TH|F'] = 'Todos los días laborales';
		vm.repetition['M|W|F'] = 'Lunes-Miercoles-viernes';
		vm.repetition['TU|TH'] = 'Martes-Jueves';
		vm.repetition['WEEK'] = 'Cada semana';
		vm.repetition['MONTH'] = 'Cada mes';
		vm.repetition['YEAR'] = 'Cada año';
		vm.dataUser = JSON.parse(sessionStorage.userCalendar);
		vm.user_id = vm.dataUser.user_id;
		vm.end_date = vm.dataUser.end_date;
		vm.campos.identificationcard = vm.dataUser.identificationcard;
		vm.tipo = 'A';

		vm.name = 'John Smith';
		$('#fullCalendar').fullCalendar('destroy');
		var $calendar = $('#fullCalendar');

		//**************************

		vm.greet = function() {
			alert(this.name);
		};
		/**
		 * @author jorge Angarita
		 * @function Agrega el nuevo evento (Tipo de Evento)
		 *
		 */
		vm.addTypeEvent = function() {

			console.info('Holaaaaaa');
			//$log.log('////jsonAddData', jsonAddData);

			if(vm.campos.nombre_actividad === '' || vm.campos.nombre_actividad === undefined){
				console.info('1');
				return false;
			}
			if(vm.campos.tiempo === '' || vm.campos.tiempo === undefined){
				console.info('2');
				return false;
			}

			var jsonAddData = {};
			jsonAddData.nombre_actividad = vm.campos.nombre_actividad.toUpperCase();
			jsonAddData.tiempo =  vm.campos.tiempo;
			jsonAddData.description= "NO TIENE";
			console.info('jsonAddData',jsonAddData);

			Api('activity/', 'POST', jsonAddData).then((function (result) {
				console.info('Api.post CRear addTypeEvent', result);
				switch (result.status) {
					case 201:
						//var idinfo  = result.data.info.length;
						Api('activity/', 'GET', null).then((function (result) {
							console.info('Api.get Lista activity', result);
							switch (result.status) {
								case 200:
									var idinfo  = result.data.info.length-1;

									vm.resultListTypeEvent.info.push(result.data.info[idinfo]);
									vm.campos.name = '';
									vm.campos.durartion = '';
									var _msg = 'Evento creado correctamente';
									Notify.send(_msg, {
										status: 'warning',
										timeout: 4000
									});

									/*$('#fullCalendar').fullCalendar('destroy');
									$calendar = $('#fullCalendar');
									$calendar.fullCalendar( 'refresh' );
									vm.createCalendar();*/
									break;
								default:
									$log.error(result.status, result.message);
									var _msg = 'Error en consulta';
									if (APP_CONFIG.DEBUG) {
										Notify.send(_msg, {
											status: 'warning',
											timeout: 7000
										});
									} else {
										Notify.send(_msg, {
											status: 'warning',
											timeout: 4000
										});
									}
							}
						}), function (error) {
							$log.error('error get login', error);
						});
						/*console.info('******addTypeEvent******', jsonAddData);
						vm.resultListTypeEvent.info.push(result.data);
						vm.campos.name = '';
						vm.campos.durartion = '';*/
						break;
					default:
						$log.error(result.status, result.message);
						var _msg = 'Error en consulta';
						if (APP_CONFIG.DEBUG) {
							Notify.send(_msg, {
								status: 'warning',
								timeout: 7000
							});
						} else {
							Notify.send(_msg, {
								status: 'warning',
								timeout: 4000
							});
						}
				}
			}), function (error) {
				$log.error('error get login', error);
			});
		};
		/**
		 * @author jorge Angarita
		 * @function Remueve el evento (Tipo de Evento)
		 *
		 */
		vm.removeTypeEvent = function(contactToRemove) {
			$log.info('contactToRemove', contactToRemove);
			Api('activity/'+contactToRemove.id, 'DELETE', null).then((function (result, status) {
				$log.log('Api.delete Eliminar Evento');

				$log.log('*******contactToRemove******', contactToRemove);
				var index = vm.resultListTypeEvent.info.indexOf(contactToRemove);
				vm.resultListTypeEvent.info.splice(index, 1);
			}), function (error) {
				$log.error('error get login', error);
			});

		};
		/**
		 * @author jorge Angarita
		 * @function Limpia los campos del evento (Tipo de Evento)
		 *
		 */
		vm.clearTypeEvent = function(contact) {
			contact.type = 'phone';
			contact.value = '';
		};
		/**
		 * @author jorge Angarita
		 * @function Abre el modal
		 *
		 */
		vm.onPerson= function() {
			$log.info('onPerson fn');
			vm.campos.descrip = '';
			$('#myModalCalendar').modal('hide');
			$('#ModalCalendarCrearagenda').modal('hide');
			$timeout(function() {
				$scope.go('app.userprofile');
			}, 400);
		};
		/**
		 * @author jorge Angarita
		 * @function Cierre el modal
		 *
		 */
		vm.onClose = function () {
			console.info('oncClose');
			$('#ModalGhostCrearagenda').modal('hide');
			$('#ModalCalendarInfoAgennda').modal('hide');
			vm.campos.descrip='';
			vm.campos.rangoHora='';
			vm.rep = false;
			vm.campos.checkboxselect = false;
			vm.campos.rephasta = false;
			vm.campos.rango = '';
		}
		/**
		 * @author jorge Angarita
		 * @function Camia de esta le variable vm.rep, para mostrar los elementos d ela lista
		 *
		 */
		vm.envenRep = function (even) {
			$log.info('envenRep', even);
			if(even===true){
				vm.rep = true;
			}else{
				vm.rep = false;
				vm.campos.rephasta = false;
			}
		};
		/**
		 * @author jorge Angarita
		 * @function verifica el rango de días paar mostar el calendario de fecha fin
		 *
		 */
		vm.envenReprango = function (even) {
			$log.info('envenReprango', even);
			switch (vm.campos.selectrep.trim()) {
				case "ALLDAY":
					$log.info('even false');
					vm.campos.rephasta = false;
				break;
				case "":
					$log.info('even false');
					vm.campos.rephasta = false;
					break;
				case undefined:
					$log.info('even false');
					vm.campos.rephasta = false;
					break;
				default:
					$log.info('even true');
					vm.campos.rephasta = true;
			}
		};
		/**
		 * @author jorge Angarita
		 * @function abre el modeal #myModalTyeEvent
		 *
		 */
		vm.EventClickTypeEevet = function (){
			$log.info('myModalTyeEvent');
			$('#myModalTyeEvent').modal('show');
		}
		/**
		 * @author jorge Angarita
		 * @function abre el modeal #myModalTyeEvent
		 *
		 */
		vm.envenInfoTypeEvent = function (event) {
			$log.info('envenInfoTypeEvent',event);
			var odjJson = JSON.parse(event);
			vm.campos.rango = '';
			vm.campos.rango = vm.TimeListD(odjJson.tiempo);

			setTimeout(function(){
				$('.selectpicker').selectpicker('refresh');
			},100);
			$log.info('vm.campos.rangoHora', vm.campos.rangoHora);


		}

		vm.clickNotify= function() {
			$log.info('clickNotify fn');
		};
		/**
		 * @author jorge Angarita
		 * @function Guarda el agendamiento por parte d elos dostores  para los eventos privados o publicos (ghost)
		 *
		 */
		vm.docrearAgenda = function (tipo) {
			$log.info('loading calendar', tipo);

			var fini;
			var ffin;
			var metodo;
			var jsonDataIn = {};
			console.info('docrearAgenda vm.campos ', vm.campos);

			/***
			 * Eventos para el calendario
			 * A => Agenda progranada
			 * G => Tiempo disponible
			 * GA => Tiempo disponible Actualizdo
			 * AD => Agenda Eliminada
			 * GD => Tiempo libre eliminado
			 ***/

			switch (tipo){
				case 'A':
					fini =  moment($('#finifinicre').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
					metodo = 'POST';

					var objClient = JSON.parse(vm.campos.cliente);
					var objType_event = JSON.parse(vm.campos.event);
					var objInfoData = {};
					objInfoData.id_cliente = objClient.id;
					objInfoData.nombre_cliente = objClient.nombre_cliente;
					objInfoData.id_actividad = objType_event.id;
					objInfoData.nombre_actividad = objType_event.nombre_actividad;
					objInfoData.tiempo_actividad = objType_event.tiempo;
					console.info('client', objClient);
					console.info('type_event', objType_event);
					console.info('infoData', objInfoData);
					jsonDataIn.init_date = fini;
					jsonDataIn.init_hour = vm.campos.hini;
					jsonDataIn.end_hour = vm.campos.hfin;
					jsonDataIn.latitude = vm.campos.cx,
					jsonDataIn.longitude = vm.campos.cy,
					jsonDataIn.ghost = false;
					jsonDataIn.description = JSON.stringify(objInfoData);
					jsonDataIn.description_id = 99999;
					break
				case 'G':
					fini =  moment($('#finig').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
					ffin = moment($('#ffing').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
					metodo = 'POST';
					jsonDataIn.init_date = fini;
					jsonDataIn.end_date = ffin;
					jsonDataIn.init_hour = vm.campos.hini;
					jsonDataIn.end_hour = vm.campos.hfin;
					jsonDataIn.ghost = true;

					var obJson = JSON.parse(vm.campos.descrip);
					jsonDataIn.description = obJson.name;
					jsonDataIn.description_id = obJson.id;
					break;
				case 'GA':
					metodo = 'POST';
					vm.campos.checkboxselect = false;
					break;
				case 'AD':
					break;
				case 'GD':
					break;
				default:
					break;
			}

			var val =  false;
			var fvalue;
			var dd = new  Date();
			var day = moment().format('YYYY-MM-DD');
			$log.info('Fecha actual', day);
			$log.info('jsonDataIn', jsonDataIn);
			if(fini >= day){
				$log.info('***fini >= day****');
					vm.MgsFini = false;
					//Validación Fechas
					if (vm.campos.checkboxselect === true) {
						switch (vm.campos.selectrep.trim()) {
							case 'ALLDAY':
								if (fini) {
									vm.MgsFfin = false;
									val = false;
									jsonDataIn.repeat = "ALLDAY";
									jsonDataIn.end_date = fini;
								} else {
									vm.desMsgFecha = "La fecha inicial nopuede ser vacia";
									vm.MgsFfin = true;
									val = true;
									return false;
								}
								break;
							case 'DAY':
								$log.info('DAY');
								if($('#fecharangos').val()){
									$log.info('******SISISISISIIS********');
									ffin = moment($('#fecharangos').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
								}
								if (ffin < fini ) {
									vm.desMsgFechaRango = "La fecha de inicio debe ser menor a la final";
									vm.MgsFfin = true;
									val = true;
									return false;
								} else {
									vm.MgsFfin = false;
									val = false;
									jsonDataIn.repeat = "DAY";
									jsonDataIn.end_date = ffin;
								}

								break;
							case "M|TU|W|TH|F":
								$log.info('M|TU|W|TH|F');
								if($('#fecharangos').val()){
									//.info('******SISISISISIIS********');
									ffin = moment($('#fecharangoGhost').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');

								}else {
									//$log.info('******NONONO********');
									ffin = moment($('#fecharango').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
								}

								//fvalue = sumerDays(fini, 7, 'd');
								if (ffin < fini) {
									vm.desMsgFechaRango = "La fecha de inicio debe ser menor a la final";
									vm.MgsFfin = true;
									val = true;
									return false;
								} else {
									vm.MgsFfin = false;
									val = false;
									jsonDataIn.repeat = "M|TU|W|TH|F";
									jsonDataIn.end_date = ffin;
								}
								break;
							case "M|W|F":
								$log.info('M|W|F');

								if($('#fecharangos').val()){
									//$log.info('******SISISISISIIS********');
									ffin = moment($('#fecharangos').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');

								}

								if (ffin < fini) {
									vm.desMsgFechaRango = "La fecha de inicio debe ser menor a la final";
									vm.MgsFfin = true;
									val = true;
									return false;
								} else {
									vm.MgsFfin = false;
									val = false;
									jsonDataIn.repeat = "M|W|F";
									jsonDataIn.end_date = ffin;
								}

								break;
							case "TU|TH":
								$log.info('TU|TH');
								if($('#fecharangos').val()){
									$log.info('******SISISISISIIS********');
									ffin = moment($('#fecharangos').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');

								}

								if (ffin < fini) {
									vm.desMsgFechaRango = "La fecha de inicio debe ser menor a la final";
									vm.MgsFfin = true;
									val = true;
									return false;
								} else {
									vm.MgsFfin = false;
									val = false;
									jsonDataIn.repeat = "TU|TH";
									jsonDataIn.end_date = ffin;
								}
								break;
							case "WEEK":
								$log.info('WEEK');
								if($('#fecharangos').val()){
									//$log.info('******SISISISISIIS********');
									ffin = moment($('#fecharangos').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');

								}

								if (ffin < fini) {
									vm.desMsgFechaRango = "La fecha de inicio debe ser menor a la final";
									vm.MgsFfin = true;
									val = true;
									return false;
								} else {
									vm.MgsFfin = false;
									val = false;
									jsonDataIn.repeat = "WEEK";
									jsonDataIn.end_date = ffin;
								}
								break;
							case "MONTH":
								$log.info('MONTH');
								if($('#fecharangos').val()){
									//$log.info('******SISISISISIIS********');
									ffin = moment($('#fecharangos').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
								}

								if (ffin < fini) {
									vm.desMsgFechaRango = "La fecha de inicio debe ser menor a la final";
									vm.MgsFfin = true;
									val = true;
									return false;
								} else {
									vm.MgsFfin = false;
									val = false;
									jsonDataIn.repeat = "MONTH";
									jsonDataIn.end_date = ffin;
								}
								break;
							case "YEAR":
								$log.info('YEAR');
								if($('#fecharangos').val()){
									//$log.info('******SISISISISIIS********');
									ffin = moment($('#fecharangos').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
								}

								if (ffin < fini) {
									vm.desMsgFechaRango = "La fecha de inicio debe ser menor a la final";
									vm.MgsFfin = true;
									val = true;
									return false;
								} else {
									vm.MgsFfin = false;
									val = false;
									jsonDataIn.repeat = "YEAR";
									jsonDataIn.end_date = ffin;
								}
								break;
							default:
								$log.info('*** default ********');
								break;

						}
					} else {
						$log.info('***jsonDataIn.repeat = "NONE"****');
						jsonDataIn.repeat = "NONE";
						jsonDataIn.end_date = fini;
					}
					//Fin Validación Fechas

					//Validación campo hora
					if (vm.campos.hini === undefined || vm.campos.hini === null) {
						vm.MgsHini = true;
						vm.MsgMgsHini = "Debe seleccionar una hora";
						return false;
					} else if (vm.campos.hini === undefined || vm.campos.hini === null) {
						vm.MgsHfin = true;
						vm.FechaHfin = "Debe seleccionar una hora";
					} else {
						vm.MgsHini = false;
						vm.MgsHfin = false;
					}


					var jsonData = {};
					jsonData.user_id = vm.user_id;
					jsonData.client_id = objInfoData.id_cliente;
					jsonData.timezones = [jsonDataIn];

					$log.info('***Info jsonData****', jsonData);
					if (val === false) {
						Api('events/', 'POST', jsonData).then((function (result) {
							$log.log('Api.post CRear Agenda', result);
							switch (result.status) {
								case 201:

									$('#createCustomer2').modal('hide');
									$('#confirmUsers').modal('hide');
									vm.rep = false;
									vm.campos.rephasta = false;
									vm.campos.checkboxselect = false;

									$('#fullCalendar').fullCalendar('destroy');
									$calendar = $('#fullCalendar');
									$calendar.fullCalendar( 'refresh' );
									vm.createCalendar();

									vm.rep = false;
									vm.campos.rephasta =false;
									break;
								default:
									$log.error(result.status, result.message);
									var _msg = 'Ocurrio un error al intentar crear el evento de agenda';
									if (APP_CONFIG.DEBUG) {
										Notify.send(_msg, {
											status: 'warning',
											timeout: 7000
										});
									} else {
										Notify.send(_msg, {
											status: 'warning',
											timeout: 4000
										});
									}
							}
						}), function (error) {
							$log.error('error get events/create', error);
						});
					}
			}else{
				vm.desMsgFechaIni = "Fecha inicial  debe ser menor ó igual a la fecha actual";
				vm.MgsFini = true;
				return false;
			}
		};


		/**
		 * @author jorge Angarita
		 * @function Elimina un Evento
		 *
		 */
		vm.doeliminarAgenda = function () {
			$log.info('loading calendar doeliminarAgenda');

			var jsonDataIn = {};


			$log.info('vm.user_id', vm.user_id);
			$log.info('vm.campos.idRegistro', vm.campos.idRegistro);
			$log.info('vm.campos.cliente', JSON.parse($( "#cliente" ).val()));
			var client = JSON.parse($( "#cliente" ).val());
			if(vm.campos.idRegistro){

				var jsonData = {};
				jsonData.client_id = client.id;
				jsonData.user_id = vm.user_id;
				jsonData.event_id = vm.campos.idRegistro;

				$log.info('***Info jsonData****', jsonData);

				Api('events/', 'PUT', jsonData).then((function (result) {
					$log.log('Api.put Eliminar Agenda', result);
					switch (result.status) {
						case 201:
							$('#createCustomer2').modal('hide');
							$('#confirmUsers').modal('hide');
							$('#fullCalendar').fullCalendar('destroy');
							$calendar = $('#fullCalendar');
							$calendar.fullCalendar( 'refresh' );
							vm.createCalendar();
							break;
						default:
							$log.error(result.status, result.message);
							var _msg = 'Ocurrio un error al intentar crear el evento de agenda';
							if (APP_CONFIG.DEBUG) {
								Notify.send(_msg, {
									status: 'warning',
									timeout: 7000
								});
							} else {
								Notify.send(_msg, {
									status: 'warning',
									timeout: 4000
								});
							}
					}
				}), function (error) {
					$log.error('error get events/create', error);
				});
			}else{
				vm.desMsgFechaIni = "El registro del evento no existe";
				vm.MgsFini = true;
				return false;
			}
		};

		/**
		 * @author jorge Angarita
		 * @function Realiza la reprogramación de un aevento agendado
		 *
		 */
		vm.doreprogramarAgenda = function (tipo) {
			$log.info('loading calendar', tipo);

			var fini;
			var ffin;
			var metodo;
			var jsonDataIn = {};
			console.info('docrearAgenda vm.campos ', vm.campos);

			/***
			 * Eventos para el calendario
			 * A => Agenda progranada
			 * G => Tiempo disponible
			 * GA => Tiempo disponible Actualizdo
			 * AD => Agenda Eliminada
			 * GD => Tiempo libre eliminado
			 ***/

			switch (tipo){
				case 'R':
					fini =  moment($('#finifinicre').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
					metodo = 'POST';

					var objClient = JSON.parse($('#cliente').val());
					var objType_event = JSON.parse($('#event').val());
					var objInfoData = {};
					objInfoData.id_cliente = objClient.id;
					objInfoData.nombre_cliente = objClient.nombre_cliente;
					objInfoData.id_actividad = objType_event.id;
					objInfoData.nombre_actividad = objType_event.nombre_actividad;
					objInfoData.tiempo_actividad = objType_event.tiempo;
					console.info('client', objClient);
					console.info('type_event', objType_event);
					console.info('infoData', objInfoData);
					jsonDataIn.init_date = fini;
					jsonDataIn.end_date = fini;
					jsonDataIn.init_hour = $('#hini').val();
					jsonDataIn.end_hour = $('#hfin').val();
					jsonDataIn.latitude = $('#cxCustomer').val();
					jsonDataIn.longitude = $('#cyCustomer').val();
					jsonDataIn.ghost = false;
					jsonDataIn.description = JSON.stringify(objInfoData);
					jsonDataIn.description_id = 99999;
					jsonDataIn.event_id =  vm.campos.idRegistro;
					break
				default:
					break;
			}

			var val =  false;
			var fvalue;
			var dd = new  Date();
			var day = moment().format('YYYY-MM-DD');
			$log.info('Fecha actual', day);
			if(fini >= day){
				$log.info('***fini >= day****');
				vm.MgsFini = false;
				//Validación Fechas

				//Fin Validación Fechas

				//Validación campo hora
				if ($('#hini').val() === undefined || $('#hini').val() === null) {
					vm.MgsHini = true;
					vm.MsgMgsHini = "Debe seleccionar una hora";
					return false;
				} else if ($('#hfin').val() === undefined || $('#hfin').val() === null) {
					vm.MgsHfin = true;
					vm.FechaHfin = "Debe seleccionar una hora";
				} else {
					vm.MgsHini = false;
					vm.MgsHfin = false;
				}



				jsonDataIn.user_id = vm.user_id;
				jsonDataIn.client_id = objInfoData.id_cliente;

				$log.info('***Info jsonDataIn****', jsonDataIn);
				if (val === false) {
					Api('events/reschedule/', 'POST', jsonDataIn).then((function (result) {
						$log.log('Api.post CRear Agenda', result);
						switch (result.status) {
							case 201:

								$('#createCustomer2').modal('hide');
								$('#confirmUsers').modal('hide');
								vm.rep = false;
								vm.campos.rephasta = false;
								vm.campos.checkboxselect = false;

								$('#fullCalendar').fullCalendar('destroy');
								$calendar = $('#fullCalendar');
								$calendar.fullCalendar( 'refresh' );
								vm.createCalendar();

								vm.rep = false;
								vm.campos.rephasta =false;
								break;
							default:
								$log.error(result.status, result.message);
								var _msg = 'Ocurrio un error al intentar crear el evento de agenda';
								if (APP_CONFIG.DEBUG) {
									Notify.send(_msg, {
										status: 'warning',
										timeout: 7000
									});
								} else {
									Notify.send(_msg, {
										status: 'warning',
										timeout: 4000
									});
								}
						}
					}), function (error) {
						$log.error('error get events/create', error);
					});
				}
			}else{
				vm.desMsgFechaIni = "Fecha inicial  debe ser menor ó igual a la fecha actual";
				vm.MgsFini = true;
				return false;
			}
		};

		vm.selectAction = function () {
			$scope.formData = {};
			console.info('selectAction', vm.campos.selectOption)
			switch (vm.campos.selectOption){
				case 'save':
					vm.docrearAgenda('A');
					break;
				case 'delete':
					vm.doeliminarAgenda();
					break;
				case 'update':
					vm.doreprogramarAgenda('R');
					break;
			}
		}
		/**
		 * @author jorge Angarita
		 * @function valida los campos del formulario, antes de crear el evento
		 *
		 */
		vm.validator = function () {
			var _msg = "";
			var val = false;
			console.info('vm.campos', vm.campos);
			if(vm.campos.direccion === '' || vm.campos.direccion === undefined){
				console.info('direccion');
				_msg = "El campo dirección es obligatorio";
				val = true;
			}else{
				val = false;
			}
			if(vm.campos.cliente === '' || vm.campos.cliente === undefined){
				console.info('cliente');
				_msg += "<br/> El campo cliente es obligatorio";
				val = true;
			}else{
				val = false;
			}
			if(vm.campos.finicre === '' || vm.campos.finicre === undefined){
				console.info('finicre');
				_msg += "<br/> El campo fecha es obligatorio";
				val = true;
			}else{
				val = false;
			}
			if(vm.campos.event === '' || vm.campos.event === undefined){
				console.info('event');
				_msg += "<br/> El campo Tipo de Evento es obligatorio";
				val = true;
			}else{
				val = false;
			}
			if(vm.campos.event !== ''){
				if(vm.campos.hini.trim()> vm.campos.hfin.trim()){
					console.info('hini');
					_msg += "La hola fin no puede ser menor a la fecha inicial";
					val = true;
				}else{
					val = false;
				}
			}


			if(vm.campos.checkboxselect === true){
				if(vm.campos.selectrep === '' || vm.campos.selectrep === undefined){
					_msg += "<br/> El campo Tipo de repetición es obligatorio";
					val = true;
				}else{
					val = false;
				}
			}


			vm.campos.validatorcam = val;
			if(val){
				Notify.send(_msg, {
					status: 'warning',
					timeout: 4000
				});
			}


		}

		vm.validatorUpdate = function () {
			var _msg = "";
			var val = false;
			console.info('vm.campos', vm.campos);
			if($("#direccion").val() === '' || $("#direccion").val() === undefined){
				console.info('direccion');
				_msg = "El campo dirección es obligatorio";
				val = true;
			}else{
				val = false;
			}
			if($("#cliente").val() === '' || $("#cliente").val() === undefined){
				console.info('cliente');
				_msg += "<br/> El campo cliente es obligatorio";
				val = true;
			}else{
				val = false;
			}
			if($("#finifinicre").val() === '' || $("#finifinicre").val() === undefined){
				console.info('finicre');
				_msg += "<br/> El campo fecha es obligatorio";
				val = true;
			}else{
				val = false;
			}
			if($("#event").val() === '' || $("#event").val() === undefined){
				console.info('event');
				_msg += "<br/> El campo Tipo de Evento es obligatorio";
				val = true;
			}else{
				val = false;
			}
			if($("#event").val() !== ''){
				if($("#hini").val().trim()> $("#hfin").val().trim()){
					console.info('hini');
					_msg += "La hola fin no puede ser menor a la fecha inicial";
					val = true;
				}else{
					val = false;
				}
			}


			/*if(vm.campos.checkboxselect === true){
				if(vm.campos.selectrep === '' || vm.campos.selectrep === undefined){
					_msg += "<br/> El campo Tipo de repetición es obligatorio";
					val = true;
				}else{
					val = false;
				}
			}*/


			vm.campos.validatorcam = val;
			if(val){
				Notify.send(_msg, {
					status: 'warning',
					timeout: 4000
				});
			}


		}

		vm.validatordata = function (tipo) {
			console.info('validatordata');
			console.info(vm.campos.hini.trim(), vm.campos.hfin.trim());
			var _msg = "";
			var val = false;

			switch (tipo.trim()){
				case 'F':
					if(vm.campos.finicre.trim()> vm.campos.fecharango.trim()){
						_msg = "La fecha de repeteición  no puede ser menor a la fecha inicial";
						val = true;
					}else{
						val = false;
					}
					break;
				case 'H':
					if(vm.campos.hini.trim()> vm.campos.hfin.trim()){
						console.info('SI');
						_msg = "La hola fin no puede ser menor a la hora inicial";
						val = true;
					}else{
						console.info('NO');
						val = false;
					}
					break;
			}

			console.info(val, ' FALSE');
			if(val){
				console.info(val, ' TRUE');
				Notify.send(_msg, {
					status: 'warning',
					timeout: 4000
				});
			}
		}


		/**
		 * @author jorge Angarita
		 * @function publicos (ghost), Agendar evento
		 *
		 */
		vm.doAgendar = function () {
			$log.info('loading doAgendar');

			var fini;
			var ffin;
			var metodo;
			var jsonDataIn = {};
			var jsonData = {};
			var _opts ={};
			//_opts.headers.header

			fini =  moment($('#ffin').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
			ffin = moment($('#ffin').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
			$log.info('Fecha fini', fini);
			$log.info('Fecha ffin', ffin);
			metodo = 'POST';

			var val =  false;
			var fvalue;
			var dd = new  Date();
			var day = moment().format('YYYY-MM-DD');
			$log.info('Fecha actual', day);
			$log.info(Date.parse(fini), Date.parse(day));
			if(Date.parse(fini)>=Date.parse(day)){
				$log.info('SISISISI', day);
					val = false;
				vm.doGeoRequest("Calle 80a # 104-49", "BOGOTA", function(request){
					$log.info('doGeoRequest', request);
					var data = request.data;

					jsonData.application_id = vm.application_id;
					jsonData.scheduling_config_id = 1;
					jsonData.medical_id = vm.session_id;
					jsonData.event_id = $('#tipeEvent').val();
					jsonData.user_id = 20;
					jsonData.date_appointment = fini;
					jsonData.init_hour = $('#hinip').val();
					jsonData.end_hour = $('#hfinip').val();
					jsonData.description_type = $('#descrip').val();
					jsonData.imei = "39475293845";
					jsonData.id_cliente = jsonData.user_id;
					jsonData.nombre_cliente = "BancoPopular";
					jsonData.direccion = vm.campos.direc;
					jsonData.telefono =  11111111;
					jsonData.cy = data.cx;
					jsonData.cx = data.cy;
					jsonData.fecha_prog_ini = fini;
					jsonData.fecha_prog_fin = ffin;
					$log.info('jsonData scheduledAppointmentsPatient', jsonData);


					/*vm.doRequestRest(jsonDataIn, function (request) {
						$log.info('jsonDataIn', request);
					});*/

					Api(vm.urlbase[tyoeUrl]+'agendamientoPaciente/scheduledAppointmentsPatient', 'POST', jsonData, {'external': true}).then((function (result) {
						$log.log('Api.post CRear Agenda', result);
						switch (result.status) {
							case 201:
								//$log.log('*************', result.data);

								$('#ModalCalendarCrearagenda').modal('hide');
								$('#ModalGhostCrearagenda').modal('hide');
								vm.rep = false;
								vm.campos.rephasta = false;
								vm.campos.checkboxselect = false;

								//setTimeout(function(){
								//$calendar.fullCalendar('destroy');

								//$('#fullCalendar').html('');
								$('#ModalCalendarInfoAgennda').modal('hide');
								$('#fullCalendar').fullCalendar('destroy');
								$calendar = $('#fullCalendar');
								$calendar.fullCalendar( 'refresh' );
								//$templateCache.removeAll();
								vm.createCalendar();
								//setInterval(vm.createCalendar(), 10000);

								vm.rep = false;
								vm.campos.rephasta =false;
								//setTimeout("location.reload()", 2000);
								//},100);
								break;
							default:
								$log.error(result.status, result.message);
								var _msg = 'Ocurrio un error al intentar crear el evento de agenda';
								if (APP_CONFIG.DEBUG) {
									Notify.send(_msg, {
										status: 'warning',
										timeout: 7000
									});
								} else {
									Notify.send(_msg, {
										status: 'warning',
										timeout: 4000
									});
								}
						}
					}), function (error) {
						$log.error('error get login', error);
					});
				});

			}else{
				vm.desMsgFechaIni = "Fecha inicial  debe ser menor ó igual a la fecha actual";
				vm.MgsFini = true;
				return false;
			}
		};
		/***
		 * **/
		vm.doGeoRequest = function (textoBusqueda, ciudad, functionCallback) {
			var data = {}
			data['city'] = ciudad;
			data['address'] = textoBusqueda;

			$.ajax({
				beforeSend: function(request) {
					request.setRequestHeader("Authorization", "Token " + "FIMZB0P7LKEIUZA6RVCW18LIPHSKNM");
				},
				type: "post",
				url: "https://sitidata-stdr.appspot.com/api/geocoder/",
				dataType: "json",
				contentType: "application/json",
				data: JSON.stringify(data),
				success: function(request) {
					//console.log("Request", request);
					(typeof(functionCallback) === 'function') ? functionCallback(request): functionCallback(null);
				},
				error: function(e, v) {
					console.log(e)
					console.log(v)
					$log.error("Hay un problema en la comunicación con el sistema de georreferenciación, Intente uevamente mas tarde", 'shake');
					try{
						ocultarEnProceso()
						setTimeout(alerta.cerrar, 5000)
					}catch (error){
						console.log("Ocurrio un error diferente al esperado: ", error)
					}
				}

			});
		}

		vm.doRequestRest = function (data, functionCallback) {

			$.ajax({
				beforeSend: function(request) {
					request.setRequestHeader("token", "fTfmTGcCzFLerwSLYsJo");
					request.setRequestHeader("Content-Type", "application/json");
					request.setRequestHeader("Access-Control-Allow-Origin", "*");
					request.setRequestHeader("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
					request.setRequestHeader("Access-Control-Max-Age", "3600");
					request.setRequestHeader("Access-Control-Allow-Headers", "X-ACCESS_TOKEN", "Access-Control-Allow-Origin", "Authorization", "Origin", "x-requested-with", "Content-Type", "Content-Range", "Content-Disposition", "Content-Description");
				},
				type: "post",
				url: "http://receptor1.sitimapa.com/service/index.php/bancopopular/InsertarRegistro",
				dataType: "json",
				contentType: "application/json",
				data: JSON.stringify(data),
				success: function(request) {
					//console.log("Request", request);
					(typeof(functionCallback) === 'function') ? functionCallback(request): functionCallback(null);
				},
				error: function(e, v) {
					$log.error("Hay un problema en la comunicación con el sistema de georreferenciación, Intente uevamente mas tarde", 'shake');
					try{
						$log.error(result.status, result.message);
					}catch (error){
						console.log("Ocurrio un error diferente al esperado: ", error);
					}
				}

			});
		}

		/**
		 * @author jorge Angarita
		 * @function Actualiza el evento publico (Ghost)
		 *
		 */
		vm.doUpdateAgendaGhost = function () {
			$log.log('doUpdateAgendaGhost');
			if (vm.campos.hini === undefined || vm.campos.hfinghost === null) {
				vm.MgsHini = true;
				vm.MsgMgsHini = "Debe seleccionar una hora";
				return false;
			} else if (vm.campos.hfin === undefined || vm.campos.hfin === null) {
				vm.MgsHfin = true;
				vm.FechaHfin = "Debe seleccionar una hora";
			} else {
				vm.MgsHini = false;
				vm.MgsHfin = false;
			}

			if (vm.campos.hini.trim() > vm.campos.hfin.trim()) {
				vm.MgsHini = true;
				vm.MsgMgsHini = "La hora de inicio debe ser menor a la fecha fin";
				return false;
			} else {
				vm.MgsIni = false;
			}

			if (vm.campos.hinighost > vm.campos.hini) {
				vm.MgsHini = true;
				vm.MsgMgsHini = "La hora de inicio debe ser mayor a la hora porgamada disponible";
				return false;
			} else {
				vm.MgsIni = false;
			}

			if (vm.campos.hfin.trim() > vm.campos.hfinghost) {
				vm.MgsHfin = true;
				vm.FechaHfin = "La hora de inicio debe ser menor a la hora porgamada disponible";
				return false;
			} else {
				vm.MgsIni = false;
			}

			var jsonData = {};
			jsonData.application_id = vm.application_id;
			jsonData.medical_id = vm.session_id;
			jsonData.init_hour = vm.campos.hini;
			jsonData.end_hour = vm.campos.hfin;
			$log.info('***jsonData****',jsonData);
			Api('http://scheduler.sitidoctor-161813.appspot.com/api/v1/events/update/'+vm.campos.idRegistro, 'PUT', jsonData, {'external': true}).then((function (result) {
				$log.log('Api.post Actualizar Ghost Agenda', result);
				switch (result.status) {
					case 201:
						//$log.log('*************', result.data);
						$('#ModalGhostCrearagenda').modal('hide');
						$('#ModalGhostCrearagenda').modal('hide');
						setTimeout(function(){
							vm.createCalendar();
						},100);
						break;
					default:
						$log.error(result.status, result.message);
						var _msg = 'Ocurrio un error al intentar crear el evento de agenda';
						if (APP_CONFIG.DEBUG) {
							Notify.send(_msg, {
								status: 'warning',
								timeout: 7000
							});
						} else {
							Notify.send(_msg, {
								status: 'warning',
								timeout: 4000
							});
						}
				}
			}), function (error) {
				$log.error('error get login', error);
			});



		}
		/**
		 * @author jorge Angarita
		 * @function hace la suma de dias, meses y años, insertandole el valor a sumar
		 *
		 */
		function sumerDays(date, cant, option) {
			$log.info('BUUUUU',date)
			var d = new Date(date);
			switch(option){
				case 'd':
					d.setDate(d.getDate() + cant);
					break;
				case 'm':
					d.setMonth(d.getMonth() + cant);
					break;
				case 'a':
					d.setFullYear(d.getFullYear() + cant);
					break;
				default:
					break;
			}

			return d;
		}
		/**
		 * @author jorge Angarita
		 * @function abre el model #ModalCalendarCrearagenda, cuando se hace click en un día del calendarío
		 *
		 */
		vm.onDayClick= function(date,jsEvent, view) {
			vm.chackrepeti = true;
			vm.tipo= 'B';
			setTimeout(function(){
				console.info('onDayClick');
				vm.loadMap();
				$("#cliente").val('');
				$('.selectpicker').selectpicker('refresh');
				$('#addressCustomer').val('');
				$('.selectpicker').selectpicker('refresh');
				$('#cxCustomer').val('');
				$('.selectpicker').selectpicker('refresh');
				$('#cyCustomer').val('');
				$('.selectpicker').selectpicker('refresh');
				$('#finifinicre').val(date);
				$('.selectpicker').selectpicker('refresh');
				$('#hini').val('');
				$('.selectpicker').selectpicker('refresh');
				$('#hfin').val('');
				$('.selectpicker').selectpicker('refresh');
				$('#event').val('');
				$('.selectpicker').selectpicker('refresh');
				$('#chackrepeti').removeClass( "ng-hide" );
				$('.selectpicker').selectpicker('refresh');


				$('#updateGroupNew').addClass( "ng-hide" );
				$('.selectpicker').selectpicker('refresh');
				$('#deleteGroupNew').addClass( "ng-hide" );
				$('.selectpicker').selectpicker('refresh');
				$('#saveGroupNew').removeClass( "ng-hide" );
				$('.selectpicker').selectpicker('refresh');

				$('#directionsgeo').addClass( "ng-hide" );
				$('.selectpicker').selectpicker('refresh')

			},100);
			$("#createCustomer2").modal();
			/*$('#createCustomer2').on('shown.bs.modal', function(e) {
				$log.info('loading modal ModalCalendarCrearagenda date' ,date);
				$log.info('loading modal ModalCalendarCrearagenda jsEvent' ,jsEvent);
				$log.info('loading modal ModalCalendarCrearagenda view' ,view);


			});*/

		};
		/**
		 * @author jorge Angarita
		 * @function abre el model #ModalCalendarCrearagenda, cuando se hace click en el boton "AGENDAR CITA"
		 *
		 */
		vm.onEventDayClick= function() {
			vm.loadMap();

			vm.tipo = 'A';
			vm.directionsgeo = false;
			vm.chackrepeti = true;
			vm.campos.direccion = '';
			vm.campos.cliente = '';
			vm.campos.event = '';
			vm.campos.cx = '';
			vm.campos.cy = '';
			vm.campos.finicre = '';
			vm.campos.checkboxselect = false;
			vm.campos.rephasta = false;
			vm.rep = false;
			vm.campos.fecharango = vm.myDate();
			vm.campos.finicre = vm.myDate();
			vm.campos.rango = '';
			vm.btnSHSave= true;
			vm.btnSHDelete= false;
			vm.btnSHUpdate= false;
			vm.btnSHUpdate=false;
			vm.btnSHDelete=false;
			//vm.myDate
			/*vm.campos.rango = vm.TimeListD(15);*/
			$("#createCustomer2").modal();


		};
		/**
		 * @author jorge Angarita
		 * @function Activar el Mapa, se debe colocar en el metodo donde se valla amostrar el Mapa
		 *
		 */
		vm.loadMap = function() {
			$("#createCustomer2").on("shown.bs.modal", function(e) {
				//Anuket.streetView(coords);
				Anuket.run('#mapCustomer2', {
					'zoom': 12,
					'latlng': { "lat": 4.6725634, "lng": -74.064028 },
					'initMarker': false
				});

				Anuket.removeMarkers();
				Anuket.addMarker({ "lat": parseFloat(4.6725634), "lng": parseFloat(-74.064028) }, true, 'createCustomer2', null);
			});
		}

		/**
		 * @author jorge Angarita
		 * @function Georeferencia la dirección y muesta latitude y longitude
		 *
		 */
		vm.onBlur = function() {
			/**
			 * A => Eventos que reconocen la variable global
			 * B => Eventos qu eno reconocen las variable y toca por Jquery
			 * **/
			console.log("onblur");

			var addressCustomer = vm.campos.direccion.split(',');
			//cll 84 24 78
			console.info('addressCustomer', addressCustomer);
			if (addressCustomer.length > 1) {

				var objSend = { city: addressCustomer[1], address: addressCustomer[0] };

				Notify.send("Buscando dirección...", {
					status: 'info',
					timeout: 3500
				});

				Anuket.moveMarkerCustomer = false;
				console.info('objSend', objSend);
				Api('sitidata/geocoder/', 'POST', objSend).then((function(result) {
					console.info('result onBlur', result);
					switch (result.status) {
						case 200:
						case 201:
							if (result.data.latitude != 0 && result.data.longitude != 0) {

								Notify.send("Geo localización exitosa..", {
									status: 'success',
									timeout: 3500
								});
								vm.campos.cx = result.data.latitude;
								$('.selectpicker').selectpicker('refresh');
								vm.campos.cy = result.data.longitude;
								$('.selectpicker').selectpicker('refresh');
								vm.directionsgeo = true;
								$('.selectpicker').selectpicker('refresh');
								/*$('#addressCustomerGeo').removeClass( "ng-hide" );
								$('.selectpicker').selectpicker('refresh');*/
								vm.campos.direccionGeo = result.data.dirtrad;
								$('.selectpicker').selectpicker('refresh');
								console.info('vm.obj.cx', vm.campos.cx);
								console.info('vm.obj.cx', vm.campos.cy);
								if(vm.tipo === 'B'){
									$('#directionsgeo').removeClass( "ng-hide" );
									$('.selectpicker').selectpicker('refresh');
									$('#direccionGeo').val(result.data.dirtrad);
									$('.selectpicker').selectpicker('refresh');
									$('#cxCustomer').val(vm.campos.cx);
									$('.selectpicker').selectpicker('refresh');
									$('#cyCustomer').val(vm.campos.cy);
									$('.selectpicker').selectpicker('refresh');
								}

								Anuket.removeMarkers();
								Anuket.addMarker({ "lat": parseFloat(result.data.latitude), "lng": parseFloat(result.data.longitude) }, true, 'createCustomer2', null);
								Anuket.centerMap(parseFloat(result.data.latitude),parseFloat(result.data.longitude));

							} else {
								vm.campos.cx = '';
								vm.campos.cy = '';
								Notify.send("Error al buscar la dirección. Por favor ubique el punto de forma manual.", {
									status: 'warning',
									timeout: 6000
								});
							}

							break;
						default:
							var _msg = 'Las credenciales de autenticación no se proporcionaron';
							Notify.send(_msg, {
								status: 'warning',
								timeout: 4500
							});
					}
				}), function(error) {
					switch (error) {
						case 400:
						case 401:
							var _msg = 'Las credenciales de autenticación no se proporcionaron';
							Notify.send(_msg, {
								status: 'warning',
								timeout: 4500
							});
							break;
						case 403:
							var _msg = 'No tiene permisos para consultar el Geo.';
							Notify.send(_msg, {
								status: 'warning',
								timeout: 4500
							});
							break;
						default:
							console.log("error inesperado. user_get");
					}
				});

			} else {
				Notify.send("La dirección no contiene el siguiente formato “dirección, ciudad” ", {
					status: 'warning',
					timeout: 7000
				});
			}
		};
		/**
		 * @author jorge Angarita
		 * @function GeoIvenrso a paratir de latitude y longitude
		 *
		 */
		vm.onGeoInverso = function() {

			console.log("onGeoInverso");

			var cx = vm.campos.cx;
			var cy = vm.campos.cy;
			if(cx === "0.0"){
				cx = "4.67248774"
			}

			if(cy === "0.0"){
				cy = "-74.06385402"
			}
			//cll 84 24 78
			console.info('addressCustomer', addressCustomer);


				var objSend = { longitude: cy, latitude: cx };

				Notify.send("Buscando dirección...", {
					status: 'info',
					timeout: 3500
				});

				Anuket.moveMarkerCustomer = false;
				console.info('objSend', objSend);
				Api('sitidata/geoinverso/', 'POST', objSend).then((function(result) {
					console.info('result', result);
					switch (result.status) {
						case 200:
						case 201:
							if (result.data.latitude != 0 && result.data.longitude != 0) {

								Notify.send("Geo localización exitosa..", {
									status: 'success',
									timeout: 3500
								});
								vm.campos.cx = cx;
								vm.campos.cy = cy;
								vm.campos.direccion = result.data.address1+","+result.data.city1;

								Anuket.removeMarkers();
								Anuket.addMarker({ "lat": parseFloat(cx), "lng": parseFloat(cy) }, true, 'createCustomer2', null);
								Anuket.centerMap(parseFloat(cx),parseFloat(cy));

							} else {
								vm.campos.cx = '';
								vm.campos.cy = '';
								Notify.send("Error al buscar la dirección. Por favor ubique el punto de forma manual.", {
									status: 'warning',
									timeout: 6000
								});
							}

							break;
						default:
							var _msg = 'Las credenciales de autenticación no se proporcionaron';
							Notify.send(_msg, {
								status: 'warning',
								timeout: 4500
							});
					}
				}), function(error) {
					switch (error) {
						case 400:
						case 401:
							var _msg = 'Las credenciales de autenticación no se proporcionaron';
							Notify.send(_msg, {
								status: 'warning',
								timeout: 4500
							});
							break;
						case 403:
							var _msg = 'No tiene permisos para consultar el Geo.';
							Notify.send(_msg, {
								status: 'warning',
								timeout: 4500
							});
							break;
						default:
							console.log("error inesperado. user_get");
					}
				});
		};
		/**
		 * @author jorge Angarita
		 * @function Mensajes de Confirmación
		 *
		 */
		vm.confirm = function(option, userId) {
			console.log("confirm",option);
			switch (option) {
				case 'save':
					vm.campos.textConfirmUsers = 'Se puese sobrelapar si existe otro evento con la misma franja horaria.';
					vm.validator();
					break;
				case 'delete':
					vm.campos.validatorcam =false;
					vm.campos.textConfirmUsers = 'Esta seguro de eliminar este evento.';
					vm.campos.deleteUserId = userId;
					break;
				case 'update':
					vm.campos.textConfirmUsers = 'Esta seguro de actualizar este usuario.';
					vm.validatorUpdate();
					break;
				default:
					console.log("error option incorrect users");
			}

			vm.campos.selectOption = option;

			if(vm.campos.validatorcam === false){
				$("#confirmUsers").modal();
			}

		};

		vm.clearForm = function() {
			console.log("clearForm");
			vm.campos = {
				direccion: '',
				cx: '',
				cy: '',
				checkboxselect: false,
				rephasta: false,
				fecharango: vm.myDate(),
				finicre: vm.myDate(),
				rango: '',
				cliente: '',
				event:''
			};

			vm.rep = false;

			/*angular.forEach(vm.listUsers, function(value, index) {

				document.getElementById("customersUsersList")[index].checked = false;

			});

			document.getElementById('cyCustomer').value = '';
			document.getElementById('cxCustomer').value = '';*/

		};

		vm.formatData = function (data) {

		}
		/**
		 * @author jorge Angarita
		 * @function abre el model #ModalCalendarInfoAgennda, cuando se hace click en un evento privado del agendamiento
		 *
		 */
		vm.onEventDayClickFunction= function(calEvent, jsEvent, view, resourceObj, color) {
			$log.info('onEventDayClickFunction ');
			$log.info('calEvent', calEvent);
			$log.info('jsEvent', calEvent);
			$log.info('view', calEvent);
			$log.info('resourceObj', calEvent);
			$log.info('view', calEvent);
			vm.loadMap();
			$("#createCustomer2").modal();
			vm.chackrepeti = false;
			vm.directionsgeo = false;
			vm.campos.fini =  calEvent._start.format('DD/MM/YYYY');
			if(calEvent._end !== null){
				vm.campos.ffin = calEvent._end.format('DD/MM/YYYY');
				$('#ffinga').val(calEvent._end.format('DD/MM/YYYY'));
			}

			var client= {};
			var actividad= {};
			client.id = calEvent.description.id_cliente;
			client.nombre_cliente = calEvent.description.nombre_cliente;
			$('#cliente').val(JSON.stringify(client));
			$('.selectpicker').selectpicker('refresh');
			actividad.id = calEvent.description.id_actividad;
			actividad.nombre_actividad = calEvent.description.nombre_actividad;
			actividad.tiempo = calEvent.description.tiempo_actividad;
			$('#event').val(JSON.stringify(actividad));
			$('.selectpicker').selectpicker('refresh');
			vm.envenInfoTypeEvent(JSON.stringify(actividad));
			$('.selectpicker').selectpicker('refresh');
			$('#finifinicre').val(calEvent._start.format('DD/MM/YYYY'));
			$('.selectpicker').selectpicker('refresh')
			setTimeout(function(){
				console.info('start', calEvent._start.format('HH:mm:ss'));
				$('#hini').val(calEvent._start.format('HH:mm:ss'));
				$('.selectpicker').selectpicker('refresh')
				$('#hfin').val(calEvent._end.format('HH:mm:ss'));
				console.info('_end', calEvent._end.format('HH:mm:ss'));
				$('.selectpicker').selectpicker('refresh');
			},100);

			vm.campos.cx = calEvent.cx;
			vm.campos.cy = calEvent.cy;

			vm.onGeoInverso();
			vm.btnSHSave=false;
			vm.btnSHUpdate=true;
			vm.btnSHDelete=true;
			vm.campos.idRegistro = calEvent.id;
		};

		/**
		 * @author jorge Angarita
		 * @function abre el model #ModalCalendarInfoAgennda, cuando se hace click en un evento privado del agendamiento
		 *
		 */
		vm.onEventDayClickLibre= function(calEvent, jsEvent, view, resourceObj, color) {
			$log.info('onEventDayClickLibre');
			$log.info('calEvent', calEvent);
			$log.info('jsEvent', jsEvent);
			$log.info('view', view);
			//moment(hini+' '+vm.datoList[i].init_hour,'YYYY-MM-DD H:m:s').format('YYYY-MM-DD')
			vm.campos.fini =  calEvent._start.format('DD/MM/YYYY');
			vm.campos.ffin = calEvent._end.format('DD/MM/YYYY');
			$('#finiga').val(calEvent._start.format('DD/MM/YYYY'));
			$('#ffinga').val(calEvent._end.format('DD/MM/YYYY'));
			$('#hinip').val(calEvent._start.format('HH:mm:ss'));
			$('#descrip').val(calEvent.description);
			$('#direc').val('');
			vm.campos.direc = "";
			$('#hfinip').val(calEvent._end.format('HH:mm:ss'));
			$('#repetition').html(calEvent.repetition);
			$('#tipeEvent').val(calEvent.id);
			$( '.btn-agenda-privada-form' ).css( "background", "linear-gradient(60deg, #"+color+", #"+color+")" );
			$('#cliente').val('cliente1');

			vm.loadMap();
			$("#createCustomer2").modal();
			vm.btnSHSave=true;
			vm.campos.direccion = '';
			vm.campos.cliente = '';
			vm.campos.event = '';
			vm.campos.cx = '';
			vm.campos.cy = '';
			vm.campos.finicre = '';
			vm.campos.checkboxselect = false;
			vm.campos.rephasta = false;
			vm.rep = false;
			document.getElementById('finifinicre').value= moment(date, 'DD/MM/YYYY H:m:s').format('DD/MM/YYYY');
			document.getElementById('fecharangos').value= moment(date, 'DD/MM/YYYY H:m:s').format('DD/MM/YYYY');
			vm.campos.rango = ''
			vm.campos.idRegistro = calEvent.id;

		};
		/**
		 * @author jorge Angarita
		 * @function abre el model #myModalCalendar
		 *
		 */
	    vm.onEventClick= function(calEvent, jsEvent, view) {

			//$('#myModalCalendar').modal('show');

	    };
		/**
		 * @author jorge Angarita
		 * @function abre el model #onEventGhostClick
		 *
		 */
		vm.onEventGhostClick= function() {
			$log.info('loading modal Ghost');
			$('#ModalGhostCrearagenda').on('shown.bs.modal', function(e) {
				$log.info('loading modal');
				setTimeout(function(){
					$('.selectpicker').selectpicker('refresh');
				},50);
			});

			vm.campos.rangoHora='';
			$('#ModalGhostCrearagenda').modal('show');
			setTimeout(function(){
				$('.selectpicker').selectpicker('refresh');
			},50);
		};
		/**
		 * @author jorge Angarita
		 * @function Muestar el calendarío con las citas agendadas
		 *
		 */
        vm.createCalendar = function() {
			$calendar.fullCalendar( 'refresh' );
			$calendar.fullCalendar( 'rerenderEvents' );
			$log.log('createCalendar');
			//setTimeout("location.reload()", 1000);
			//******
			$('#gifImage').html("<img ng-if='mostrarCargando' id='mostrarCargando' src='images/gifitarami.gif'/>");


			$log.info('loading jsonData', jsonData);
			$log.info('loading calendar', $calendar);
			var programacionA = null;
			console.info('vm.myDate2()',vm.myDate2());
			var jsonData = {
				"user_id": vm.user_id,
				"end_date": vm.myDate2()
			};

			$log.info('loading jsonData', jsonData);
			$log.info('loading calendar', $calendar);
			var programacionA = null;
			setTimeout(function(){
				$('.selectpicker').selectpicker('refresh');
				//Api('http://servicesadmin.praxis-medic.appspot.com/api/agendamientoMedico/ListAvailableEventsMedical', 'POST', jsonData, {'external': true}).then((function(result) {
				Api('scheduler/list/', 'POST', jsonData).then((function(result) {
					$log.log('List createCalendar', result );
					switch (result.status) {
						case 200:
							$('#gifImage').html('');
							vm.datoList = result.data.info;
							var a = [];
							//$log.log('.includes()', vm.datoList.includes("Odontologia"));
							//vm.datoList.splice( 2, 1 );
							console.info("jQuery.isEmptyObject(result.data)**", jQuery.isEmptyObject(result.data));
							// Verifica si el json de respuesta esta vacio
							if(jQuery.isEmptyObject(result.data)){
								a = [];
							}else{
								for(var i = 0; i<vm.datoList.length;i++){
									var b = {};
									$log.log('Lista Array vm.datoList[i]**********', vm.datoList[i]);
									var hini = vm.datoList[i].date_prog
									var eventList = vm.datoList[i];

									if((vm.datoList[i].ghost === "0" || vm.datoList[i].ghost === "None") &&  vm.datoList[i].schedstatus_id === "2") {
										$log.log('*********2 AGENDADO********', vm.datoList[i].description.length);
										var description = JSON.parse(vm.datoList[i].description);
										b.id = vm.datoList[i].id;
										b.user_id = vm.datoList[i].user_id;
										b.title = description.nombre_actividad;
										b.color = '#02a0ba';
										b.type = 'AGENDADO';
										//b.repetition = vm.repetition['DAY'];
										b.description = description;
										b.cx = vm.datoList[i].latitude;
										b.cy = vm.datoList[i].longitude;
										b.start = moment(hini+' '+vm.datoList[i].init_hour,'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
										b.end = moment(hini+' '+vm.datoList[i].end_hour,'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
										b.allDay = false;

									}else if((vm.datoList[i].ghost === "0" || vm.datoList[i].ghost === "None") &&  vm.datoList[i].schedstatus_id === "5"){
										$log.log('*********5 RE-PROGRAMADO********',vm.datoList[i].description.length);
										//if(vm.datoList[i].description.length >20){
										var description = JSON.parse(vm.datoList[i].description);
										b.id = vm.datoList[i].id;
										b.user_id = vm.datoList[i].user_id;
										b.title = description.nombre_actividad;
										b.color = '#7CBB0F';
										b.type = 'REAGENDADA';
										b.description = description;
										b.cx = vm.datoList[i].latitude;
										b.cy = vm.datoList[i].longitude;
										b.start = moment(hini+' '+vm.datoList[i].init_hour,'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
										b.end = moment(hini+' '+vm.datoList[i].end_hour,'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
										b.allDay = false;
										//}

									}else {
										$log.log('*********vm.datoList[i].ghost********', vm.datoList[i].ghost);
										$log.log('*********vm.datoList[i].schedstatus_id********', vm.datoList[i].schedstatus_id);
									}

									a[i] = b;
								}
							}



							$log.info('createCalendar fn');
							var programacion = a;
							programacionA = a
							$log.info('*** programacion ***', programacion);
							var today = new Date();
							var y = today.getFullYear();
							var m = today.getMonth();
							var d = today.getDate();

							$calendar.fullCalendar('removeEvents');
							$calendar.fullCalendar('refresh');
							$calendar.fullCalendar({
								viewRender: function(view, element) {

								},
								locale: 'es',
								//lang: 'es',

								header: {
									left: 'title',
									center: 'month,agendaWeek,agendaDay',
									right: 'today,prev,next'
								},

								defaultDate: today,
								selectable: true,
								selectHelper: true,
								editable: true,
								businessHours: true,
								eventLimit: true, // allow "more" link when too many events

								views: {
									month: { // name of view
										titleFormat: 'MMMM YYYY'
										// other view-specific options here
									},
									week: {
										titleFormat: " MMMM D YYYY"
									},
									day: {
										titleFormat: 'D MMM, YYYY'
									}
								},
								dayClick: function(date, jsEvent, view) {
									vm.onDayClick(date.format('DD/MM/YYYY'), jsEvent, view);
								},
								eventClick: function(calEvent, jsEvent, view, resourceObj) {

									var color;
									switch(calEvent.type){
										case 'AGENDADO':
											$log.info('*******AGENDADO******',calEvent.type);
											color = "02a0ba";
											vm.onEventDayClickFunction(calEvent, jsEvent, view, resourceObj, color);
											break;
										default:
											break;
									}

								},
								events: programacion
							});
							$calendar.fullCalendar('rerenderEvents' );
							$calendar.fullCalendar( 'refetchEvents' );


							break;
						default:
							$log.error(result.status, result.message);
							var _msg='Ocurrio un error al intentar ';
							if (APP_CONFIG.DEBUG) {
								Notify.send(_msg, {
									status: 'warning',
									timeout: 7000
								});
							} else {
								Notify.send(_msg, {
									status: 'warning',
									timeout: 4000
								});
							}
					}
				}), function(error) {
					//$log.error('error get list description', error);
				});
			},1000);

        }; //createCalendar
		/**
		 * @author jorge Angarita
		 * @function agendamientoMedico/SchedulerConfig/
		 *
		 */
		vm.listdes = function () {

			Api(vm.urlbase[tyoeUrl]+'agendamientoMedico/SchedulerConfig/'+vm.application_id, 'GET', null, {'external': true}).then((function(result) {
			//Api('agendamientoMedico/SchedulerConfig/'+vm.application_id, 'GET').then((function(result) {
				$log.log('Api.get Descripcuón ******************');
				switch (result.status) {
					case 200:
						//$log.error('Lista descripción', result.data);
						var dataList = result.data;
						var arraydesp = [];
						console.info('++++dataList++++', dataList);
						for(var ind=0; ind<dataList.length; ind++){
							var objDesp = {};
							objDesp.id = dataList[ind].id;
							objDesp.name = dataList[ind].name;
						}
						vm.listaDesciones = arraydesp;
						$log.info('Lista descripción****', vm.listaDesciones);

						break;
					default:
						$log.error(result.status, result.message);
						var _msg='Ocurrio un error al intentar mostar la lista de descripciones';
						if (APP_CONFIG.DEBUG) {
							Notify.send(_msg, {
								status: 'warning',
								timeout: 7000
							});
						} else {
							Notify.send(_msg, {
								status: 'warning',
								timeout: 4000
							});
						}
				}
			}), function(error) {
				$log.error('error get login', error);
			});
		};
		/**
		 * @author jorge Angarita
		 * @function Lista los tipo de eventos por medico, Ppara tablas y combos
		 *
		 */
		vm.ListTypeEvent = function () {
			$log.log('*****LIATA TIPO DE EVENTO********');
			Api('activity/', 'GET', null).then((function(result) {
			//Api('agendamientoMedico/ListTypeAppointmentId/'+vm.session_id, 'GET', null).then((function (result) {
				//$log.log('LIATA TIPO DE EVENTO', result);
				switch (result.status) {
					case 200:
						$log.log('*****result.data********', result.data);;
						vm.resultListTypeEvent = result.data;
						var dataList = result.data.info;
						var arraydesp = [];
						//$log.log('*****dataList********', dataList);
						for(var ind=0; ind<dataList.length; ind++){
							var objDesp = {};
							objDesp.id = dataList[ind].id;
							objDesp.nombre_actividad = dataList[ind].nombre_actividad;
							objDesp.tiempo = dataList[ind].tiempo;
							arraydesp[ind] = objDesp;
						}
						vm.listaEvent = arraydesp;
						console.info('vm.listaEvent', vm.listaEvent)

						break;
					default:
						$log.error(result.status, result.message);
						var _msg = 'Ocurrio un error al intentar crear el evento de agenda';
						if (APP_CONFIG.DEBUG) {
							Notify.send(_msg, {
								status: 'warning',
								timeout: 7000
							});
						} else {$log.info('Lista descripción', vm.listaDesciones);
							Notify.send(_msg, {
								status: 'warning',
								timeout: 4000
							});
						}
				}
			}), function (error) {
				$log.error('error get login', error);
			});
		}

		/**
		 * @author jorge Angarita
		 * @function Lista de horas por el ranga dinamico
		 *
		 */
		vm.TimeListD = function (mun) {

			var listDatos = [];
			var ini =  0;
			// fecha inicial
			var d = new Date();
			d.setHours(1)
			d.setMinutes(0);
			d.setSeconds(0)
			//$log.info('***** listDatos d ****', d);
			var fini = moment(d).format('HH:mm:ss');
			// fecha final
			var f = new Date();
			f.setHours(23)
			f.setMinutes(0);
			f.setSeconds(0)
			//$log.info('***** listDatos f ****', f);
			var ffin = moment(f).format('HH:mm:ss');
			/*$log.info('***** Hora Inicio ****', moment(d).format('HH:mm:ss'));
			$log.info('***** Hora Final ****', ffin);
			$log.info('***** Minutos a sumar ****', mun);*/
			listDatos[ini] = fini;

			do{
				ini++;
				d.setMinutes(d.getMinutes() + mun);
				listDatos[ini] = moment(d).format('HH:mm:ss');
			}while(listDatos[ini] < ffin);

			return listDatos;
		};
		/**
		 * @author jorge Angarita
		 * @function Formato de fecha con la función Date()
		 *
		 */
		vm.myDate = function () {

			var hoy = new Date();
			var dd = hoy.getDate();
			var mm = hoy.getMonth() + 1; //hoy es 0!
			var yyyy = hoy.getFullYear();

			if (dd < 10) {
				dd = '0' + dd;
			}

			if (mm < 10) {
				mm = '0' + mm;
			}

			return dd + '/' + mm + '/' + yyyy;
		};

		vm.myDate2 = function () {

			var hoy = new Date();
			var dd = hoy.getDate();
			var mm = hoy.getMonth() + 1; //hoy es 0!
			var yyyy = hoy.getFullYear();

			if (dd < 10) {
				dd = '0' + dd;
			}

			if (mm < 10) {
				mm = '0' + mm;
			}


			return yyyy + '-' + mm + '-' + dd;
		};
		/**
		 * @author jorge Angarita
		 * @function Colocar la primera letra de cada palabra en mayuscula
		 *
		 */
		vm.ucWords= function (string){
			var arrayWords;
			var returnString = "";
			var len;
			console.info('ucWords',string);
			arrayWords = string.split(" ");
			len = arrayWords.length;
			for(var i=0;i < len ;i++){
				if(i != (len-1)){
					returnString = returnString+vm.ucFirst(arrayWords[i])+" ";
				}
				else{
					returnString = returnString+vm.ucFirst(arrayWords[i]);
				}
			}
			return returnString;
		}
		/**
		 * @author jorge Angarita
		 * @function Segunda para de Colocar la primera letra de cada palabra en mayuscula
		 *
		 */
		vm.ucFirst= function(string){
			return string.substr(0,1).toUpperCase()+string.substr(1,string.length).toLowerCase();
		}
		/**
		 * @author jorge Angarita
		 * @function carga las funciones del calendario
		 *
		 */
		vm.loadDatePicker = function(){
			$log.info('loading loadDatePicker');

			var $datepicker = $('.datepicker');

			$datepicker.datetimepicker({
	            format: 'DD/MM/YYYY',
	            icons: {
	                time: "fa fa-clock-o",
	                date: "fa fa-calendar",
	                up: "fa fa-chevron-up",
	                down: "fa fa-chevron-down",
	                previous: 'fa fa-chevron-left',
	                next: 'fa fa-chevron-right',
	                today: 'fa fa-screenshot',
	                clear: 'fa fa-trash',
	                close: 'fa fa-remove',
	                inline: true
	            }
	        });
		};

		vm.session = function () {
			vm.getAll();
		};

		vm.getAll = function () {
			$log.info('userCalendar', JSON.parse(sessionStorage.userCalendar));
			vm.typeuser = sessionStorage.typeuser;
			vm.ListTypeEvent();
			vm.createCalendar();
			//vm.listTypeEvent();
			//vm.campos.rango = vm.TimeListD(15);
			vm.myDate();
			vm.loadDatePicker();
		}

        vm.main = function() {
			//setTimeout("location.reload()", 5000);
			if (sessionStorage.session != undefined) {
				/*$calendar.fullCalendar({
					monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
					monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
					dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
					dayNamesShort: ['Dom','Lun','Mar','Mié','Jue','Vie','Sáb']
				});*/
				/*$calendar.fullCalendar({
					locale: 'es'
				})*/
				vm.campos.nameuser = vm.ucWords(vm.dataUser.nameuser);
				APP_CONFIG.AUTH2_TOKEN = JSON.parse(sessionStorage.user).data.token.access_token;
				vm.session();
				vm.typeuser = sessionStorage.typeuser;
			} else {

                $state.go('app.slogin');
            };

		};

        vm.main();
    } //calendarController

    angular.module('app.core').controller('calendarController', calendarController);
    calendarController.$inject = [
        '$scope',
		'$cacheFactory',
        '$state',
        '$timeout',
        '$log',
        'Api',
        'Notify',
        'Alerts',
        'APP_CONFIG'
    ];
})();
