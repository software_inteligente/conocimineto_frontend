/*
 * gruops controller here
 *
 */
(function() {
	'use strict';

	function itemsController($scope, $timeout, $log, Api, Notify, APP_CONFIG, $state) {
		var vm = this;
		vm.id = ''

		vm.goCalendar = function(id, namearea) {

			var obj = JSON.stringify({"id": id,"name": namearea});
			sessionStorage.setItem("objInfo",obj);
			console.info('ueueueueuendjndjjn')
			$state.go("app.formularios");

		};

		vm.getObj = function () {
			var objInfo = JSON.parse(sessionStorage.objInfo);

			if(Object.entries(objInfo).length>0){
				vm.id = '/'+objInfo.id;
			}else{
				vm.id = '';
			}
		};

		vm.getItems = function() {
			Api('http://localhost/proyectos/SAS/conocimiento/backEnd_Conocimiento/public/api/Items'+vm.id, 'GET', null, {'external': true}).then((function(result) {
				//Api('user/', 'GET').then((function(result) {
				switch (result.status) {
					case 200:
						console.info('result++++++', result)
						//$('#usersCalendarTable').DataTable().destroy();
						vm.listItems = result.data;
						console.log("list users: ",vm.listUsers);

						var arrayAvatar = [
							'images/avatars/man.svg','images/avatars/man-1.svg','images/avatars/man-2.svg','images/avatars/man-3.svg',
							'images/avatars/woman.svg','images/avatars/woman-1.svg','images/avatars/woman-2.svg','images/avatars/woman-3.svg'
						];

						var cont = 0;
						angular.forEach(vm.listItems, function(value, index){

							if(cont==7){
								cont=0;
							}

							value.avatar = arrayAvatar[cont];

							cont++;

						});

						setTimeout(function() {

							$('#usersCalendarTable').DataTable({
								"language": {
									"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
								},
								"searching": true,
								"paging": false,
								"info": false,
								"scrollX": false,
								"ordering": false
							});

						}, 100);

						break;
					default:
						var _msg = 'Las credenciales de autenticación no se proporcionaron';
						Notify.send(_msg, {
							status: 'warning',
							timeout: 4500
						});
				}
			}), function(error) {
				switch (error) {
					case 400:
					case 401:
						var _msg = 'Las credenciales de autenticación no se proporcionaron';
						Notify.send(_msg, {
							status: 'warning',
							timeout: 4500
						});
						break;
					default:
						console.log("error inesperado. user_get");
				}
			});
		};

		vm.main = function() {

			if (sessionStorage.session != undefined) {

				APP_CONFIG.AUTH2_TOKEN = JSON.parse(sessionStorage.user).data.token.access_token;
				vm.getItems();

			} else {
				$state.go('app.slogin');
			};
		};

		vm.main();
	} //usersCalendarController

	angular.module('app.core').controller('itemsController', itemsController);
	itemsController.$inject = [
		'$scope',
		'$timeout',
		'$log',
		'Api',
		'Notify',
		'APP_CONFIG',
		'$state'
	];
})();
