/* routes config here*/
(function() {
    'use strict';

    function routesConfig($stateProvider, $locationProvider, $urlRouterProvider, helper) {
        $locationProvider.html5Mode(false);
        $urlRouterProvider.otherwise('/app/login');
        $stateProvider.state('app', {
            url: '/app',
            abstract: true,
            templateUrl: helper.basepath('app.html')
        }).state('app.dashboard', {
            url: '/dashboard',
            title: 'Dashboard',
            templateUrl: helper.basepath('dashboard.html'),
            controller: 'dashController',
            controllerAs: 'dashCtrl',
            resolve: helper.resolveFor('Chartist')
        }).state('app.basemap', {
            url: '/maps',
            title: 'maps',
            templateUrl: helper.basepath('basemap.html'),
            controller: 'baseMapController',
            controllerAs: 'bmapCtrl',
            resolve: helper.resolveFor('Maps', 'MapStyles', 'Dtables')
        }).state('app.groups', {
            url: '/groups',
            title: 'groups',
            templateUrl: helper.basepath('users/groups.html'),
            controller: 'groupsController',
            controllerAs: 'gpCtrl',
            resolve: helper.resolveFor('Dtables')
        }).state('app.customers', {
            url: '/customers',
            title: 'customers',
            templateUrl: helper.basepath('users/customers.html'),
            controller: 'customersController',
            controllerAs: 'ctmsCtrl',
            resolve: helper.resolveFor('Maps','Dtables')
        }).state('app.users', {
            url: '/users',
            title: 'users',
            templateUrl: helper.basepath('users/users.html'),
            controller: 'usersController',
            controllerAs: 'usersCtrl',
            resolve: helper.resolveFor('Dtables')
        }).state('app.profiles', {
            url: '/profiles',
            title: 'Perfiles',
            templateUrl: helper.basepath('examples/profiles.html'),
            controller: 'profilesController',
            controllerAs: 'prflsCtrl'
        }).state('app.slogin', {
            url: '/login',
            title: 'login',
            templateUrl: helper.basepath('login.html'),
            controller: 'loginController',
            controllerAs: 'lgnCtrl'
        }).state('app.calendar', {
			url: '/calendar',
			title: 'calendar',
			templateUrl: helper.basepath('administration/calendar.html'),
			controller: 'calendarController',
			controllerAs: 'clndrCtrl',
			resolve: helper.resolveFor('Maps','Dtables', 'Calendar', 'SelectBootstrap')
		}).state('app.areas', {
			url: '/areas',
			title: 'areas',
			templateUrl: helper.basepath('administration/areas.html'),
			controller: 'areasController',
			controllerAs: 'uCldrCtrl',
			resolve: helper.resolveFor('Dtables')
		}).state('app.temas', {
			url: '/temas',
			title: 'temas',
			templateUrl: helper.basepath('administration/temas.html'),
			controller: 'temasController',
			controllerAs: 'temasCtrl',
			resolve: helper.resolveFor('Dtables')
		}).state('app.items', {
			url: '/items',
			title: 'items',
			templateUrl: helper.basepath('administration/items.html'),
			controller: 'itemsController',
			controllerAs: 'itemsCtrl',
			resolve: helper.resolveFor('Dtables')
		}).state('app.problematicas', {
			url: '/problematicas',
			title: 'problematicas',
			templateUrl: helper.basepath('administration/problematicas.html'),
			controller: 'problematicasController',
			controllerAs: 'probleCtrl',
			resolve: helper.resolveFor('Dtables')
		}).state('app.formularios', {
			url: '/formularios',
			title: 'formularios',
			templateUrl: helper.basepath('administration/formularios.html'),
			controller: 'formulariosController',
			controllerAs: 'formCtrl',
			resolve: helper.resolveFor('Dtables')
		});
    }

    angular.module('app.routes').config(routesConfig);
    routesConfig.$inject = [
        '$stateProvider',
        '$locationProvider',
        '$urlRouterProvider',
        'RouteHelpersProvider'
    ];
})();
